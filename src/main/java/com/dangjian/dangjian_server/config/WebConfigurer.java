package com.dangjian.dangjian_server.config;

import com.dangjian.dangjian_server.util.LoginInterceptor;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @version 1.0
 */
@Configuration
public class WebConfigurer implements WebMvcConfigurer {
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        System.out.println("拦截器配置");
        registry.addInterceptor(new LoginInterceptor())
                .addPathPatterns("/**")
                .excludePathPatterns("/adminView/login.html")
                .excludePathPatterns("/test")
                .excludePathPatterns("/error")
                .excludePathPatterns("/announcement/*")
                .excludePathPatterns("/conference/*")
                .excludePathPatterns("/student/*")
                .excludePathPatterns("/template/*")
                .excludePathPatterns("/dict/*")
                .excludePathPatterns("/wxApi/*")
                .excludePathPatterns("/admin/login")
                .excludePathPatterns("/adminView/css/**")
                .excludePathPatterns("/adminView/img/**")
                .excludePathPatterns("/adminView/jquery/**")
                .excludePathPatterns("/adminView/js/**")
                .excludePathPatterns("/clientView/**"); // 前端页面不拦截
    }
}
