package com.dangjian.dangjian_server.config;

/**
 * @author
 * 封装微信永久数据
 * 个人微信公众平台的信息 主要用于小程序登录
 */
public class WechatConfig {
    // 请求的网址
    public static final String WX_LOGIN_URL = "https://api.weixin.qq.com/sns/jscode2session";
    // appid
    public static final String APP_ID="wxc81b97d96b07f025";
    // 密匙
    public static final String APP_SECRET="17dcbeb1902629e9decea4960af5691f";
    // 固定参数
    public static final String WX_LOGIN_GRANT_TYPE = "authorization_code";
}
