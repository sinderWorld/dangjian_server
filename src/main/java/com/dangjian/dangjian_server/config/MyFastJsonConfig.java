package com.dangjian.dangjian_server.config;

import com.alibaba.fastjson.serializer.SerializerFeature;
import com.alibaba.fastjson.support.config.FastJsonConfig;
import com.alibaba.fastjson.support.spring.FastJsonHttpMessageConverter;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

/**
 * 使用阿里的fastjson需要配置相关信息
 *
 * 不同于Gson，fastjson继承完成后需要提供相应的httpMessageConverter才能使用
 * 配置fastjson的HttpMessageConverter
 * 配置完需要设置一下响应编码，否则返回json中文会乱码-->application.properties
 */
@Configuration
public class MyFastJsonConfig implements WebMvcConfigurer {
    @Override
    public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
        System.out.println("---初始化配置fastjson---");
        FastJsonHttpMessageConverter converter=new FastJsonHttpMessageConverter();
        List<MediaType> supportedMediaTypes = new ArrayList<>();
        supportedMediaTypes.add(MediaType.APPLICATION_JSON);
//        supportedMediaTypes.add(MediaType.APPLICATION_JSON_UTF8);
        supportedMediaTypes.add(MediaType.APPLICATION_ATOM_XML);
        supportedMediaTypes.add(MediaType.APPLICATION_FORM_URLENCODED);
        supportedMediaTypes.add(MediaType.APPLICATION_OCTET_STREAM);
        supportedMediaTypes.add(MediaType.APPLICATION_PDF);
        supportedMediaTypes.add(MediaType.APPLICATION_RSS_XML);
        supportedMediaTypes.add(MediaType.APPLICATION_XHTML_XML);
        supportedMediaTypes.add(MediaType.APPLICATION_XML);
        supportedMediaTypes.add(MediaType.IMAGE_GIF);
        supportedMediaTypes.add(MediaType.IMAGE_JPEG);
        supportedMediaTypes.add(MediaType.IMAGE_PNG);
        supportedMediaTypes.add(MediaType.TEXT_EVENT_STREAM);
        supportedMediaTypes.add(MediaType.TEXT_HTML);
        supportedMediaTypes.add(MediaType.TEXT_MARKDOWN);
        supportedMediaTypes.add(MediaType.TEXT_PLAIN);
        supportedMediaTypes.add(MediaType.TEXT_XML);
        converter.setSupportedMediaTypes(supportedMediaTypes);
        //创建配置类
        FastJsonConfig config=new FastJsonConfig();
        //JSON的解析过程，设置日期格式
        config.setDateFormat("yyyy-MM-dd");
        //数据编码
        config.setCharset(Charset.forName("UTF-8"));
        config.setSerializerFeatures(
                //是否在生成json时输出类名
//                SerializerFeature.WriteClassName,
                //是否输出value为null的数据
                SerializerFeature.WriteMapNullValue,
                //生成的json格式化
                SerializerFeature.PrettyFormat,
                //空集合输出[]而不是null
                SerializerFeature.WriteNullListAsEmpty,
                //空字符串输出""，而不是null
                SerializerFeature.WriteNullStringAsEmpty
        );
        converter.setFastJsonConfig(config);
        converters.add(converter);
    }
}
