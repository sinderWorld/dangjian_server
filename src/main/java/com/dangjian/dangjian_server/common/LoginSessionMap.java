package com.dangjian.dangjian_server.common;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author
 * 用于保存用户的 session_key openId/sessionId 保存登录状态
 */
public class LoginSessionMap {

    private static final Map<String, Map<String, String>> SESSION = new ConcurrentHashMap<>();

    /**
     * 添加登录session_key,openip
     * @param loginSession
     * @param map
     */
    public static void put(String loginSession, Object map) {
        SESSION.put(loginSession, (Map<String, String>) map);
    }

    /**
     * 判断是否存在key,也就是是否登录
     * @param loginSession
     * @return
     */
    public static boolean containsKey(String loginSession) {
        return SESSION.containsKey(loginSession);
    }

    /**
     * 获取
     * @param loginSession
     * @return
     */
    public static Map<String, String> get(String loginSession) {
        return SESSION.get(loginSession);
    }

    /**
     * 清除过期session
     * @param loginSession
     */
    public static void clear(String loginSession) {
        SESSION.remove(loginSession);
    }
}
