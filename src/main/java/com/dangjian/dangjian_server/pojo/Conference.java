package com.dangjian.dangjian_server.pojo;

import java.io.Serializable;
import lombok.Data;
import lombok.ToString;

/**
 * tb_conference 支部会议信息表
 * @author 
 */
@Data
@ToString
public class Conference implements Serializable {
    /**
     * 主键
     */
    private Integer con_id;

    /**
     * 会议名称
     */
    private String meetingName;

    /**
     * 会议发布人-绑定系统操作员id
     */
    private Integer adm_id;

    /**
     * 会议主持人
     */
    private String meetingHost;

    /**
     * 会议时间，格式为yyyymmdd-mm:ss:xx
     */
    private String meetingTime;

    /**
     * 会议地点
     */
    private String meetingPlace;

    /**
     * 会议主题/概要
     */
    private String conferenceTheme;

    /**
     * 会议说明(大文本)，写明与会人员，现场人员签到。理论来说定位签到
     */
    private String meetingDesc;

    /**
     * 创建时间，格式为yyyymmdd
     */
    private String createTime;

    private static final long serialVersionUID = 1L;
}