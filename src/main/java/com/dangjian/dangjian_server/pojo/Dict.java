package com.dangjian.dangjian_server.pojo;

import java.io.Serializable;
import lombok.Data;
import lombok.ToString;

/**
 * tb_dict 字典应用信息表
 * @author 
 */
@Data
@ToString
public class Dict implements Serializable {
    /**
     * 主键
     */
    private Integer dictId;

    /**
     * 显示值
     */
    private String showvalue;

    /**
     * 隐藏代码对应值
     */
    private String code;

    /**
     * 所属字段值
     */
    private String columnvalue;

    /**
     * 关联信息，XLC001-XLC002，填父级字段
     */
    private String relevance;

    /**
     * 所属类型，拼音首字母大写表示，例如训练场/XLC
     */
    private String belong;

    /**
     * 创建时间，格式为yyyymmdd
     */
    private String createtime;

    private static final long serialVersionUID = 1L;
}