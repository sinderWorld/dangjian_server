package com.dangjian.dangjian_server.pojo;

import java.io.Serializable;
import lombok.Data;
import lombok.ToString;

/**
 * tb_student 学生用户信息
 * @author 
 */
@Data
@ToString
public class Student implements Serializable {
    /**
     * 主键
     */
    private Integer stu_id;

    /**
     * 微信openId，昵称，头像根据实时更新获取(ZD才有)
     */
    private String wxOpenId;

    /**
     * 学号
     */
    private String stuNumber;

    /**
     * 姓名
     */
    private String stuName;

    /**
     * 所在党支部-计算机学院(固定)
     */
    private String branch;

    /**
     * 所在学院-字典项应用-字典项应用
     */
    private String xy001;

    /**
     * 所在专业-关联学院-字典项应用
     */
    private String zy001;

    /**
     * 班级-关联专业-字典项应用
     */
    private String bj001;

    /**
     * 性别
     */
    private String gender;

    /**
     * 民族
     */
    private String nation;

    /**
     * 籍贯
     */
    private String hometown;

    /**
     * 身份证号码
     */
    private String idenNumber;

    /**
     * 入党申请时间，格式为yyyymmdd，能填积极分子信息
     */
    private String partyAppliTime;

    /**
     * 入党积极分子时间，格式为yyyymmdd，能填发展对象信息
     */
    private String partyActivistTime;

    /**
     * 发展对象时间，格式为yyyymmdd，能填预备信息
     */
    private String depTargetTime;

    /**
     * 入党时间，格式为yyyymmdd
     */
    private String partyTime;

    /**
     * 转正时间，格式为yyyymmdd
     */
    private String turnPositiveTime;

    /**
     * 联系方式
     */
    private String phone;

    /**
     * 创建时间，格式为yyyymmdd
     */
    private String createTime;

    /**
     * 更新时间，格式为yyyymmdd
     */
    private String updateTime;

    private static final long serialVersionUID = 1L;
}