package com.dangjian.dangjian_server.pojo;

import java.io.Serializable;
import lombok.Data;
import lombok.ToString;

/**
 * tb_devobject
 * 固定信息收集模板表_发展对象信息收集表
 * @author 
 */
@ToString
@Data
public class Devobject implements Serializable {
    /**
     * 主键
     */
    private Integer dev_id;

    /**
     * 收集表状态表id
     */
    private Integer sta_id;

    /**
     * 收集表结构标题-如第27期发展对象信息收集表
     */
    private String infoTitle;

    /**
     * 班级-字典项应用
     */
    private String bj001;

    /**
     * 学号
     */
    private String stuNumber;

    /**
     * 姓名
     */
    private String stuName;

    /**
     * 性别
     */
    private String gender;

    /**
     * 籍贯
     */
    private String hometown;

    /**
     * 民族-字典项应用
     */
    private String mz001;

    /**
     * 身份证号码
     */
    private String idenNumber;

    /**
     * 曾任、现任职务
     */
    private String currentJob;

    /**
     * 有无补考
     */
    private String isTest;

    /**
     * 入党申请时间，格式为yyyymmdd
     */
    private String partyTime;

    /**
     * 确定为积极分子时间，格式为yyyymmdd
     */
    private String partyActivistTime;

    /**
     * 党校结业时间，格式为yyyymmdd
     */
    private String graduationTime;

    /**
     * 最近两学期成绩排名
     */
    private String scoRanking;

    /**
     * 最近两学期总测排名
     */
    private String comRanking;

    /**
     * 获奖情况-大文本varchar
     */
    private String awards;

    /**
     * 联系方式
     */
    private String phone;

    /**
     * 填写时间，格式为yyyymmdd
     */
    private String fillinTime;

    /**
     * 创建时间，格式为yyyymmdd
     */
    private String createTime;

    private static final long serialVersionUID = 1L;
}