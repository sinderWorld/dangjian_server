package com.dangjian.dangjian_server.pojo;

import lombok.Data;

import java.util.List;

/**
 * 分页 数据封装
 * @param <T>
 */
@Data
public class Pager<T> {
    private int page;//分页起始页
    private int size;//每页记录数
    private List<T> data;//返回的记录集合
    private long count;//总记录条数
}
