package com.dangjian.dangjian_server.pojo;

import java.io.Serializable;
import lombok.Data;
import lombok.ToString;

/**
 * tb_announcement 支部公告信息表(重要公示)
 * @author 
 */
@Data
@ToString
public class Announcement implements Serializable {
    /**
     * 主键
     */
    private Integer ann_id;

    /**
     * 公告标题
     */
    private String annTitle;

    /**
     * 公告简要
     */
    private String briefAnn;

    /**
     * 公告内容(大文本)
     */
    private String annContent;

    /**
     * 公告发布人-绑定系统操作员id
     */
    private Integer adm_id;

    /**
     * 发布时间，格式为yyyymmdd
     */
    private String releaseTime;

    /**
     * 创建时间，格式为yyyymmdd
     */
    private String createTime;

    private static final long serialVersionUID = 1L;
}