package com.dangjian.dangjian_server.pojo;

import java.io.Serializable;
import lombok.Data;

/**
 * tb_status
 * 固定信息收集模板表_信息收集状态管理表
 * @author 
 */
@Data
public class Status implements Serializable {
    /**
     * 主键
     */
    private Integer sta_id;

    /**
     * 收集表类型-字典项(积极分子，发展对象，预备党员)
     */
    private String sj001;

    /**
     * 信息收集表Id
     * 0为积极分子，1为发展对象，2为预备党员
     */
    private Integer tb_id;

    /**
     * 是否暂停收集(0已停止，1正在收集)
     */
    private String status;

    private String statusValue;

    /**
     * 发起时间yyyymmdd
     */
    private String initiateTime;

    /**
     * 截止时间yyyymmdd
     */
    private String createTime;

    private static final long serialVersionUID = 1L;
}