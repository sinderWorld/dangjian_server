package com.dangjian.dangjian_server.pojo;

import java.io.Serializable;
import lombok.Data;
import lombok.ToString;

/**
 * tb_activist
 * 固定信息收集模板表_积极分子信息收集表
 * @author 
 */
@ToString
@Data
public class Activist implements Serializable {
    /**
     * 主键
     */
    private Integer act_id;

    /**
     * 收集表状态表id
     */
    private Integer sta_id;

    /**
     * 收集表结构标题-如第27期积极分子信息收集表
     */
    private String infoTitle;

    /**
     * 班级-字典项应用
     */
    private String bj001;

    /**
     * 学号
     */
    private String stuNumber;

    /**
     * 姓名
     */
    private String stuName;

    /**
     * 性别
     */
    private String gender;

    /**
     * 籍贯
     */
    private String hometown;

    /**
     * 民族-字典项应用
     */
    private String mz001;

    /**
     * 身份证号码
     */
    private String idenNumber;

    /**
     * 曾任、现任职务
     */
    private String currentJob;

    /**
     * 入党申请时间，格式为yyyymmdd
     */
    private String partyTime;

    /**
     * 确定为积极分子时间，格式为yyyymmdd
     */
    private String partyActivistTime;

    /**
     * 获奖情况-大文本varchar
     */
    private String awards;

    /**
     * 联系方式
     */
    private String phone;

    /**
     * 填写时间，格式为yyyymmdd
     */
    private String fillinTime;

    /**
     * 创建时间，格式为yyyymmdd
     */
    private String createTime;

    private static final long serialVersionUID = 1L;
}