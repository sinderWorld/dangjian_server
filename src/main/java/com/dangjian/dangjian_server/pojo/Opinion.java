package com.dangjian.dangjian_server.pojo;

import java.io.Serializable;
import lombok.Data;
import lombok.ToString;

/**
 * tb_opinion 公示意见信息收集表
 * @author 
 */
@Data
@ToString
public class Opinion implements Serializable {
    /**
     * 主键
     */
    private Integer opi_id;

    /**
     * 公告id
     */
    private Integer ann_id;
    /**
     * 公告意见对应的公告标题
     */
    private String annTitle;

    /**
     * 意见说明(文本)
     */
    private String opinion;

    /**
     * 联系方式(可选)
     */
    private String phone;

    /**
     * 意见提出人微信openid
     */
    private String wxOpenId;

    /**
     * 创建时间，格式为yyyymmdd
     */
    private String createTime;

    /**
     * 公开收集意见截止时间yyyymmdd,默认0.-0.
     */
    private String byTime;

    private static final long serialVersionUID = 1L;
}