package com.dangjian.dangjian_server.pojo;

import java.io.Serializable;
import lombok.Data;
import lombok.ToString;

/**
 * tb_admin 系统操作员信息表
 * @author 
 */
@Data
@ToString
public class Admin implements Serializable {
    /**
     * 主键
     */
    private Integer adm_id;

    /**
     * 操作账号
     */
    private String account;

    /**
     * 密码
     */
    private String password;

    /**
     * 权限(普通管理员，高级管理员，禁用账户)字典项应用，qx001
     */
    private String qx001;
    /**
     * 权限值
     */
    private String qx001Value;

    /**
     * 最后操作时间yyyymmdd-hh:mm:ss
     */
    private String operatTime;

    /**
     * 账户创建时间yyyymmdd
     */
    private String createTime;

    private static final long serialVersionUID = 1L;
}