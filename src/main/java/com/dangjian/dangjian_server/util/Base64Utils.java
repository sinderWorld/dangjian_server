package com.dangjian.dangjian_server.util;

import java.io.UnsupportedEncodingException;
import java.util.Base64;

/**
 * @author
 * 新增密码加密功能，用于防止手机明文登录
 * base64加密
 */
public class Base64Utils {
    public static String setBase64(String pwd) throws UnsupportedEncodingException {
        Base64.Encoder encoder=Base64.getEncoder();
        return encoder.encodeToString(pwd.getBytes("UTF-8"));
    }
}
