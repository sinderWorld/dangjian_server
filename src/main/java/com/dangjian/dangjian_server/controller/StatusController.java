package com.dangjian.dangjian_server.controller;

import com.dangjian.dangjian_server.common.ResponseMessage;
import com.dangjian.dangjian_server.pojo.Announcement;
import com.dangjian.dangjian_server.pojo.Pager;
import com.dangjian.dangjian_server.pojo.Status;
import com.dangjian.dangjian_server.service.StatusService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * 信息收集 发布 管理
 */
@Slf4j
@RestController
@RequestMapping(value = "status",produces = "application/json; charset=utf-8")
public class StatusController {
    @Autowired
    private StatusService statusService;

    /**
     * 分页查询支部会议信息
     * @param page
     * @param limit
     * @return
     */
    @GetMapping(value = "getPageStatusInfo")
    public ResponseMessage getPageStatusInfo(int page, int limit) {
        Pager<Status> pager = statusService.findByPager(page,limit);

        System.out.println(pager);

        if (pager.getCount() == 0) {
            return ResponseMessage.createBySuccess("暂无信息",pager);
        } else {
            return ResponseMessage.createBySuccess(pager);
        }
    }

    /**
     * 模糊查询
     * @param page
     * @param limit
     * @param keyWork
     * @return
     */
    @GetMapping(value = "getKeyWorkStatusInfo")
    public ResponseMessage getKeyWorkStatusInfo(int page,int limit, String keyWork){
        Pager<Status> pager = statusService.findByKey(page,limit,keyWork);
        if (pager.getCount() == 0) {
            return ResponseMessage.createBySuccess("无该信息,请检查",pager);
        } else {
            return ResponseMessage.createBySuccess(pager);
        }
    }

    /**
     * 删除一条记录
     * @param sta_id
     * @return
     */
    @PostMapping(value = "delByIdStatus")
    public ResponseMessage delByIdStatus(int sta_id){
        int code = statusService.deleteByPrimaryKey(sta_id);
        return ResponseMessage.createBySuccess(code);
    }

    /**
     * 查看信息
     * @param sta_id
     * @return
     */
    @GetMapping(value = "getByIdStatus")
    public ResponseMessage getByIdStatus(int sta_id) {
        Status status = statusService.selectByPrimaryKey(sta_id);
        System.out.println("信息：" + status);
        if (status == null) {
            return ResponseMessage.createByErrorMessage("暂无此信息");
        } else {
            return ResponseMessage.createBySuccess("查看信息",status);
        }
    }

    /**
     * 更新记录
     * @param status
     * @return
     */
    @PostMapping(value = "updateByIdStatus")
    public ResponseMessage updateByIdStatus(@RequestBody Status status){
        System.out.println("获取的status信息：" + status.toString());
        int code = statusService.updateByPrimaryKeySelective(status);
        if (code == 0) {
            return ResponseMessage.createByErrorMessage("更新失败");
        } else {
            return ResponseMessage.createBySuccess("更新成功", code);
        }
    }

    /**
     * 发布新的信息收集模板
     * @param status
     * @return
     */
    @PostMapping(value = "addStatusInfo")
    public ResponseMessage addStatusInfo(@RequestBody Status status) {
        System.out.println("获取的数据：" + status.toString());

        DateFormat df = new SimpleDateFormat("yyyyMMdd");
        Calendar calendar = Calendar.getInstance();
        String initiateTime = df.format(calendar.getTime());

        // 封装数据
        status.setInitiateTime(initiateTime);// 发布时间

        int code = statusService.insertSelective(status);
        return ResponseMessage.createBySuccess("发布成功");
    }
}
