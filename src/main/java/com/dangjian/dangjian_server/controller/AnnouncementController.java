package com.dangjian.dangjian_server.controller;

import com.dangjian.dangjian_server.common.ResponseMessage;
import com.dangjian.dangjian_server.pojo.Announcement;
import com.dangjian.dangjian_server.pojo.Conference;
import com.dangjian.dangjian_server.pojo.Pager;
import com.dangjian.dangjian_server.service.AnnouncementService;
import com.dangjian.dangjian_server.service.OpinionService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * 支部公告管理
 */
@Slf4j
@RestController
@RequestMapping(value = "announcement",produces = "application/json; charset=utf-8")
public class AnnouncementController {
    @Autowired
    private AnnouncementService announcementService;

    @Autowired
    private OpinionService opinionService;

    /**
     * 分页查询支部会议信息
     * @param page
     * @param limit
     * @return
     */
    @GetMapping(value = "getPageAnnouncementInfo")
    public ResponseMessage getPageAnnouncementInfo(int page, int limit) {
        Pager<Announcement> pager = announcementService.findByPager(page,limit);

        System.out.println(pager);

        if (pager.getCount() == 0) {
            return ResponseMessage.createBySuccess("暂无公告信息",pager);
        } else {
            return ResponseMessage.createBySuccess(pager);
        }
    }

    /**
     * 模糊查询
     * @param page
     * @param limit
     * @param keyWork
     * @return
     */
    @GetMapping(value = "getKeyWorkAnnouncementInfo")
    public ResponseMessage getKeyWorkAnnouncementInfo(int page,int limit, String keyWork){
        Pager<Announcement> pager = announcementService.findByKey(page,limit,keyWork);
        if (pager.getCount() == 0) {
            return ResponseMessage.createBySuccess("无该公告信息,请检查",pager);
        } else {
            return ResponseMessage.createBySuccess(pager);
        }
    }

    /**
     * 删除一条记录
     * @param ann_id
     * @return
     */
    @PostMapping(value = "delByIdAnn")
    public ResponseMessage delByIdAnn(int ann_id){
        int code = announcementService.deleteByPrimaryKey(ann_id);
        // 这里需要加上删除意见信息 根据公告id
        int code1 = opinionService.deleteByPrimaryKey(ann_id);
        return ResponseMessage.createBySuccess(code);
    }

    /**
     * 批量删除
     * @param array
     * @return
     */
    @PostMapping(value = "delByBatchAnn")
    public ResponseMessage delByBatchAnn(int[] array) {
        int code = announcementService.deleteByBatchAnn(array);
        // 这里需要加上批量删除意见信息 根据公告id
        int code1 = opinionService.deleteByBatchOpi(array);
        return ResponseMessage.createBySuccess(code);
    }

    /**
     * 查看公告信息
     * @param ann_id
     * @return
     */
    @GetMapping(value = "getByIdAnn")
    public ResponseMessage getByIdAnn(int ann_id) {
        Announcement announcement = announcementService.selectByPrimaryKey(ann_id);
        System.out.println("公告信息：" + announcement);
        if (announcement == null) {
            return ResponseMessage.createByErrorMessage("查无此公告信息");
        } else {
            return ResponseMessage.createBySuccess("查看公告信息",announcement);
        }
    }

    /**
     * 更新记录
     * @param announcement
     * @return
     */
    @PostMapping(value = "updateByIdAnn")
    public ResponseMessage updateByIdAnn(@RequestBody Announcement announcement){
        System.out.println("获取的announcement信息：" + announcement.toString());
        int code = announcementService.updateByPrimaryKeySelective(announcement);
        if (code == 0) {
            return ResponseMessage.createByErrorMessage("更新失败");
        } else {
            return ResponseMessage.createBySuccess("更新成功", code);
        }
    }

    /**
     * 添加新公告信息
     * @param announcement
     * @return
     */
    @PostMapping(value = "addAnnouncementInfo")
    public ResponseMessage addAnnouncementInfo(@RequestBody Announcement announcement){
        DateFormat df = new SimpleDateFormat("yyyyMMdd");
        Calendar calendar = Calendar.getInstance();
        String createTime = df.format(calendar.getTime());
        // 封装创建时间
        announcement.setCreateTime(createTime);
        // 封装发布时间
        announcement.setReleaseTime(createTime);
        // 还得封装admin_id进去

        System.out.println(announcement.toString());
        int code = announcementService.insertSelective(announcement);
        return ResponseMessage.createBySuccess("新增成功",code);
    }
}
