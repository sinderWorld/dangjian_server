package com.dangjian.dangjian_server.controller;

import com.dangjian.dangjian_server.util.VerifyCode;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.awt.image.BufferedImage;
import java.io.IOException;

@Controller
public class TestController {
    // 验证码测试
    @GetMapping("test")
    public void test(HttpServletRequest request,HttpServletResponse response) throws IOException {
        VerifyCode vc = new VerifyCode();
        BufferedImage image = vc.getImage();// 获取一次性验证码
        System.out.println("验证码的文本：" + vc.getText());
        // 把文本保存到session中，为LoginServlet验证做准备
        request.getSession().setAttribute("vCode", vc.getText());
        VerifyCode.output(image, response.getOutputStream()); //把图片写到指定流中

    }
}
