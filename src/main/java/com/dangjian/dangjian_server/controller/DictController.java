package com.dangjian.dangjian_server.controller;

import com.dangjian.dangjian_server.common.ResponseMessage;
import com.dangjian.dangjian_server.pojo.Dict;
import com.dangjian.dangjian_server.service.DictService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * 字典项管理 控制器
 */
@RestController
@RequestMapping(value = "dict",produces = "application/json; charset=utf-8")
public class DictController {
    @Autowired
    private DictService dictService;

    /**
     * 根据类型belong 查询字典值
     * @param belong
     * @return
     */
    @GetMapping("getByBelong")
    public ResponseMessage getByBelong(String belong){
        List<Dict> dictList = dictService.getByBelong(belong);
        System.out.println("字典值：" + dictList);
        return ResponseMessage.createBySuccess(dictList);
    }
}
