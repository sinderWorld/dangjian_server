package com.dangjian.dangjian_server.controller;

import com.dangjian.dangjian_server.common.ResponseMessage;
import com.dangjian.dangjian_server.pojo.Admin;
import com.dangjian.dangjian_server.service.AdminService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * 用于后台管理系统登录使用
 */
@Slf4j
@RestController
@RequestMapping("admin")
public class LoginController {
    @Autowired
    private AdminService adminService;

    /**
     * 管理员登录
     * @param session
     * @return
     */
    @PostMapping("login")
    public ResponseMessage sinderLogin(HttpSession session, String account, String password, String vcCode) throws IOException {
        System.out.println("登录信息："+ account + ":" + password + ":" + vcCode);
        // 不存在该用户信息 跳转回登录页面
        HttpServletResponse response = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getResponse();
        // 判断登录
        Admin admin = adminService.login(account,password);
        // 验证码判断登录
        String vcCodeTo = vcCode.toLowerCase(); // 转为小写
        String vc = (String) session.getAttribute("vCode");
        System.out.println(session.getAttribute("vCode"));
        if (vcCodeTo.equals(vc.toLowerCase())){
            System.out.println("验证码正确");
            // 判断是否有登录信息
            if (admin == null) {
                System.out.println(admin);
                return ResponseMessage.createByErrorMessage("请输入正确的账号或者密码");
            }else {
                // 已经登录
                System.out.println("登录信息" + admin);
                session.setAttribute("adminInfo",admin);
                // 保持两个小时
                session.setMaxInactiveInterval(7200);
                return ResponseMessage.createBySuccess();
            }
        } else {
            return ResponseMessage.createByErrorMessage("验证码错误");
        }

        // session在浏览器关闭后还是会保留session信息，只是重新打开的浏览器生成的sessionId无法匹配
    }

    /**
     * 退出登录
     * @param session
     * @return
     */
    @GetMapping("exitLogin")
    public ResponseMessage exitLogin(HttpSession session){
        // 退出登录，判断是否有信息
        if (session.getAttribute("adminInfo") == null){
            // 未登录
            return ResponseMessage.createBySuccessMessage("信息丢失，请重新登录");
        } else {
            // 销毁session登录信息
            session.invalidate();
            return ResponseMessage.createBySuccessMessage("安全退出");
        }
    }

    /**
     * 判断登录账号的权限
     * @return
     */
    @GetMapping("isCode")
    public ResponseMessage isCode(HttpSession session){
        Admin admin = (Admin) session.getAttribute("adminInfo");
        if (admin.getQx001().equals("002")){
            log.info("高权限用户，具备使用管理辅导员");
            return ResponseMessage.createBySuccess();
        } else if (admin.getQx001().equals("001")){
            log.info("普通管理用户，允许正常使用");
            return ResponseMessage.createByError();
        } else if(admin.getQx001().equals("003")){
            log.info("禁用用户，不允许使用");
            return ResponseMessage.createByError();
        } else {
            log.info("不存在该用户，不允许使用");
            return ResponseMessage.createByError();
        }
    }
    /**
     * 返回用户信息
     */
    @GetMapping("adminInfo")
    public ResponseMessage adminInfo(HttpSession session){
        Admin admin = (Admin) session.getAttribute("adminInfo");
        if (admin == null) {
            return ResponseMessage.createByErrorMessage("无该管理员信息，请先退出重新登录");
        } else {
            return ResponseMessage.createBySuccess(admin);
        }
    }

    /**
     * 更新密码
     */
    @PostMapping("upPassword")
    public ResponseMessage upPassword(@RequestBody Admin admin) {
        int code = adminService.updateByPrimaryKeySelective(admin);
        System.out.println(code);
        return ResponseMessage.createBySuccess("密码更新成功");
    }
}
