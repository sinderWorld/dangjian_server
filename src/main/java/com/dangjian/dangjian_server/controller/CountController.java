package com.dangjian.dangjian_server.controller;

import com.dangjian.dangjian_server.common.ResponseMessage;
import com.dangjian.dangjian_server.dao.CountDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;

/**
 * 数据汇总分析
 */
@RestController
@RequestMapping(value = "count",produces = "application/json; charset=utf-8")
public class CountController {
    @Autowired
    private CountDao countDao;

    @GetMapping(value = "dataCount")
    public ResponseMessage dataCount(){
        // 初始化数据为-1 表示汇总出错
        int activistCount = -1;
        int depTargetCount = -1;
        int partyCount = -1;
        int turnPositiveCount = -1;

        // 获取数据
        activistCount = countDao.activistCount();
        depTargetCount = countDao.depTargetCount();
        partyCount = countDao.partyCount();
        turnPositiveCount = countDao.turnPositiveCount();
        // 封装数据
        HashMap<String, Integer> dataCount = new HashMap<>();
        dataCount.put("activistCount", activistCount);
        dataCount.put("depTargetCount", depTargetCount);
        dataCount.put("partyCount", partyCount);
        dataCount.put("turnPositiveCount", turnPositiveCount);

        return ResponseMessage.createBySuccess("党建数据分析汇总",dataCount);
    }
}
