package com.dangjian.dangjian_server.controller;

import com.dangjian.dangjian_server.common.ResponseMessage;
import com.dangjian.dangjian_server.dao.ActivistDao;
import com.dangjian.dangjian_server.dao.DevobjectDao;
import com.dangjian.dangjian_server.dao.PropartymemberDao;
import com.dangjian.dangjian_server.pojo.*;
import com.dangjian.dangjian_server.service.ActivistService;
import com.dangjian.dangjian_server.service.DevobjectService;
import com.dangjian.dangjian_server.service.PropartymemberService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 信息收集 模板信息 添加，校验，移除，查看
 */
@Slf4j
@RestController
@RequestMapping(value = "template",produces = "application/json; charset=utf-8")
public class TemplateController {
    // 积极分子
    @Autowired
    private ActivistDao activistDao;
    // 发展对象
    @Autowired
    private DevobjectDao devobjectDao;
    // 预备党员
    @Autowired
    private PropartymemberDao propartymemberDao;

    @Autowired
    private ActivistService activistService;

    @Autowired
    private DevobjectService devobjectService;

    @Autowired
    private PropartymemberService propartymemberService;

    /**
     * 校验是否已经存在学生
     * @param sta_id
     * @param tb_id
     * @param stuNumber
     * @return
     */
    @PostMapping(value = "checkTemplate")
    public ResponseMessage checkTemplate(int sta_id,int tb_id,String stuNumber){
        // 开始校验
        if (tb_id == 0) {
            Activist activist = activistDao.checkIsStu(sta_id,stuNumber);
            if (activist == null) {
                return ResponseMessage.createBySuccessMessage("校验通过");
            } else {
                return ResponseMessage.createByErrorMessage("该学生已添加");
            }
        }else if (tb_id == 1) {
            Devobject devobject = devobjectDao.checkIsStu(sta_id,stuNumber);
            if (devobject == null) {
                return ResponseMessage.createBySuccessMessage("校验通过");
            } else {
                return ResponseMessage.createByErrorMessage("该学生已添加");
            }
        }else if (tb_id == 2) {
            Propartymember propartymember = propartymemberDao.checkIsStu(sta_id,stuNumber);
            if (propartymember == null) {
                return ResponseMessage.createBySuccessMessage("校验通过");
            } else {
                return ResponseMessage.createByErrorMessage("该学生已添加");
            }
        } else {
            return ResponseMessage.createByErrorMessage("不存在该表，请联系管理员");
        }
    }

    /**
     * 添加 模板 学生 信息
     * @param sta_id
     * @param tb_id
     * @param stuName
     * @param stuNumber
     * @param gender
     * @return
     */
    @PostMapping(value = "addTemplate")
    public ResponseMessage addTemplate(int sta_id,int tb_id,String stuName,String stuNumber,String gender){
        // 开始新增
        if (tb_id == 0) {
            Activist activist = new Activist();
            activist.setSta_id(sta_id);
            activist.setStuName(stuName);
            activist.setStuNumber(stuNumber);
            activist.setGender(gender);
            int code0 = activistDao.insertSelective(activist);
            return ResponseMessage.createByErrorMessage("积极分子_添加成功");
        }else if (tb_id == 1) {
            Devobject devobject = new Devobject();
            devobject.setSta_id(sta_id);
            devobject.setStuName(stuName);
            devobject.setStuNumber(stuNumber);
            devobject.setGender(gender);
            int code1 = devobjectDao.insertSelective(devobject);
            return ResponseMessage.createByErrorMessage("发展对象_添加成功");
        }else if (tb_id == 2) {
            Propartymember propartymember = new Propartymember();
            propartymember.setSta_id(sta_id);
            propartymember.setStuName(stuName);
            propartymember.setStuNumber(stuNumber);
            propartymember.setGender(gender);
            int code2 = propartymemberDao.insertSelective(propartymember);
            return ResponseMessage.createByErrorMessage("预备党员_添加成功");
        } else {
            return ResponseMessage.createByErrorMessage("添加失败，请联系管理员");
        }
    }

    /**
     * 分页查询信息
     * @param page
     * @param limit
     * @return
     */
    @GetMapping(value = "getPageTemplateInfo")
    public ResponseMessage getPageTemplateInfo(int page, int limit,int tb_id,int sta_id) {
        System.out.println(tb_id + ":" + sta_id);
        if (tb_id == 0) {
            Pager<Activist> pager = activistService.findByPager(page,limit,sta_id);
            return ResponseMessage.createBySuccess("积极分子_查询成功",pager);
        }else if (tb_id == 1) {
            Pager<Devobject> pager = devobjectService.findByPager(page,limit,sta_id);
            return ResponseMessage.createBySuccess("发展对象_查询成功",pager);
        }else if (tb_id == 2) {
            Pager<Propartymember> pager = propartymemberService.findByPager(page,limit,sta_id);
            return ResponseMessage.createBySuccess("预备党员_查询成功功",pager);
        } else {
            return ResponseMessage.createByErrorMessage("添加失败，请联系管理员");
        }
    }

    /**
     * 删除一条记录
     * @param stuNumber
     * @return
     */
    @PostMapping(value = "delByStuNumberTemplate")
    public ResponseMessage delByStuNumberTemplate(String stuNumber,int tb_id){
        if (tb_id == 0) {
            int code0 = activistService.deleteByPrimaryKey(stuNumber);
            return ResponseMessage.createBySuccessMessage("移除成功");
        }else if (tb_id == 1) {
            int code1 = devobjectService.deleteByPrimaryKey(stuNumber);
            return ResponseMessage.createBySuccessMessage("移除成功");
        }else if (tb_id == 2) {
            int code2 = propartymemberService.deleteByPrimaryKey(stuNumber);
            return ResponseMessage.createBySuccessMessage("移除成功");
        } else {
            return ResponseMessage.createByErrorMessage("删除失败，请联系管理员");
        }
    }

    /**
     * 积极分子信息填写
     * @param activist
     * @return
     */
    @PostMapping(value = "activistAdd")
    public ResponseMessage activistAdd(@RequestBody Activist activist) {
        int code = activistService.updateByPrimaryKeySelective(activist);
        return ResponseMessage.createBySuccessMessage("填写成功");
    }

    /**
     * 发展对象信息填写
     * @param devobject
     * @return
     */
    @PostMapping(value = "devobjectAdd")
    public ResponseMessage activistAdd(@RequestBody Devobject devobject) {
        int code = devobjectService.updateByPrimaryKeySelective(devobject);
        return ResponseMessage.createBySuccessMessage("填写成功");
    }

    /**
     * 预备党员信息填写
     * @param propartymember
     * @return
     */
    @PostMapping(value = "propartymemberAdd")
    public ResponseMessage activistAdd(@RequestBody Propartymember propartymember) {
        int code = propartymemberService.updateByPrimaryKeySelective(propartymember);
        return ResponseMessage.createBySuccessMessage("填写成功");
    }

    /**
     * 查看积极分子信息
     * @param sta_id
     * @param stuNumber
     * @return
     */
    @GetMapping(value = "getActivist")
    public ResponseMessage getActivist(int sta_id,String stuNumber){
        Activist activist = activistDao.selectInfoByNumber(sta_id,stuNumber);
        return ResponseMessage.createBySuccess("积极分子信息",activist);
    }

    /**
     * 查看发展对象信息
     * @param sta_id
     * @param stuNumber
     * @return
     */
    @GetMapping(value = "getDevobject")
    public ResponseMessage getDevobject(int sta_id,String stuNumber){
        Devobject devobject = devobjectDao.selectInfoByNumber(sta_id,stuNumber);
        return ResponseMessage.createBySuccess("查看发展对象信息",devobject);
    }

    /**
     * 查看预备党员信息
     * @param sta_id
     * @param stuNumber
     * @return
     */
    @GetMapping(value = "getPropartymember")
    public ResponseMessage getPropartymember(int sta_id,String stuNumber){
        Propartymember propartymember = propartymemberDao.selectInfoByNumber(sta_id,stuNumber);
        return ResponseMessage.createBySuccess("查看预备党员信息",propartymember);
    }
}
