package com.dangjian.dangjian_server.controller;

import com.dangjian.dangjian_server.common.ResponseMessage;
import com.dangjian.dangjian_server.dao.TemplateDao;
import com.dangjian.dangjian_server.pojo.Devobject;
import com.dangjian.dangjian_server.pojo.Pager;
import com.dangjian.dangjian_server.pojo.Status;
import com.dangjian.dangjian_server.pojo.Student;
import com.dangjian.dangjian_server.service.StudentService;
import jdk.management.jfr.FlightRecorderMXBean;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 党员管理
 */
@Slf4j
@RestController
@RequestMapping(value = "student",produces = "application/json; charset=utf-8")
public class StudentController {

    @Autowired
    private StudentService studentService;

    @Autowired
    private TemplateDao templateDao;

    /**
     * 分页查询党员信息
     * @param page
     * @param limit
     * @return
     */
    @GetMapping(value = "getPageStudentInfo")
    public ResponseMessage getPageStudentInfo(int page, int limit) {
        Pager<Student> pager = studentService.findByPager(page,limit);

        System.out.println(pager);

        if (pager.getCount() == 0) {
            return ResponseMessage.createBySuccess("暂无党员信息",pager);
        } else {
            return ResponseMessage.createBySuccess(pager);
        }
    }

    /**
     * 模糊查询
     * @param page
     * @param limit
     * @param keyWork
     * @return
     */
    @GetMapping(value = "getKeyWorkStudentInfo")
    public ResponseMessage getKeyWorkStudentInfo(int page,int limit, String keyWork){
        Pager<Student> pager = studentService.findByKey(page,limit,keyWork);
        if (pager.getCount() == 0) {
            return ResponseMessage.createBySuccess("无该人员信息,请检查",pager);
        } else {
            return ResponseMessage.createBySuccess(pager);
        }
    }

    /**
     * 选择条件查询
     * @param page
     * @param limit
     * @param col
     * @param code
     * @return
     */
    @GetMapping(value = "findByCondition")
    public ResponseMessage findByCondition(int page, int limit,String col,String code){
        Pager<Student> pager = studentService.findByCondition(page,limit,col,code);
        if (pager.getCount() == 0) {
            return ResponseMessage.createBySuccess("无该人员信息,请检查",pager);
        } else {
            return ResponseMessage.createBySuccess(pager);
        }
    }

    /**
     * 删除一条记录
     * @param stuId
     * @return
     */
    @PostMapping(value = "delByIdStu")
    public ResponseMessage delByIdStu(int stuId){
        int code = studentService.deleteByPrimaryKeyStu(stuId);
        return ResponseMessage.createBySuccess(code);
    }

    /**
     * 批量删除
     * @param array
     * @return
     */
    @PostMapping(value = "delByBatchStu")
    public ResponseMessage delByBatchStu(int[] array) {
        int code = studentService.deleteByBatchStu(array);
        return ResponseMessage.createBySuccess(code);
    }

    /**
     * 学生绑定注册
     * @param student
     * @return
     */
    @PostMapping(value = "addStudentInfo")
    public ResponseMessage addStudentInfo(@RequestBody Student student) {
        System.out.println("获取的数据：" + student.toString());

        DateFormat df = new SimpleDateFormat("yyyyMMdd");
        Calendar calendar = Calendar.getInstance();
        String createTime = df.format(calendar.getTime());

        // 封装数据
        student.setCreateTime(createTime);// 创建时间

        int code = studentService.insertSelective(student);
        return ResponseMessage.createBySuccess("添加学生成功");
    }

    /**
     * 查看学生信息
     * @param stu_id
     * @return
     */
    @GetMapping(value = "getByIdStu")
    public ResponseMessage getByIdStu(int stu_id) {
        Student student = studentService.selectByPrimaryKeyStu(stu_id);
        System.out.println("学生信息：" + student);
        if (student == null) {
            return ResponseMessage.createByErrorMessage("查无此人");
        } else {
            return ResponseMessage.createBySuccess("查看学生信息",student);
        }
    }

    /**
     * 查看学生信息
     * @param stuNumber
     * @return
     */
    @GetMapping(value = "getByStuNumber")
    public ResponseMessage getByStuNumber(String stuNumber) {
        Student student = studentService.selectByStuNumber(stuNumber);
        System.out.println("学生信息：" + student);
        if (student == null) {
            return ResponseMessage.createByErrorMessage("你还没登记，请先登记再使用！");
        } else {
            // 已经登记 从各个模板表中查询
            List<Status> activists = templateDao.getActivist(stuNumber);
            List<Status> devobjects = templateDao.getDevobject(stuNumber);
            List<Status> propartymembers = templateDao.getPropartymember(stuNumber);

            HashMap<String,Object> hashMap = new HashMap<>();

            hashMap.put("activists", activists);
            hashMap.put("devobjects", devobjects);
            hashMap.put("propartymembers", propartymembers);

            return ResponseMessage.createBySuccess("已经登记",hashMap);
        }
    }

    /**
     * 更新记录
     * @param student
     * @return
     */
    @PostMapping(value = "updateByIdStu")
    public ResponseMessage updateByIdStu(@RequestBody Student student){
        System.out.println("获取的student信息：" + student.toString());
        int code = studentService.updateByPrimaryKeySelective(student);
        if (code == 0) {
            return ResponseMessage.createByErrorMessage("更新失败");
        } else {
            return ResponseMessage.createBySuccess("更新成功", code);
        }
    }


    /**
     * 分页查询 积极分子/发展对象/预备党员 信息
     * @param page
     * @param limit
     * @return
     */
    @GetMapping(value = "getPageStudentInfoBy")
    public ResponseMessage getPageStudentInfoBy(int page, int limit, int tb_id) {
        // 判断是查 积极分子/发展对象/预备党员 信息
        // 0为积极分子，1为发展对象，2为预备党员
        // 初始化
        Pager<Student> pager = null;
        if (tb_id == 0) {
            pager = studentService.findByPagerByPartyA(page,limit);
        } else if (tb_id == 1){
            pager = studentService.findByPagerByDep(page,limit);
        } else if (tb_id == 2) {
            pager = studentService.findByPagerByParty(page,limit);
        }

        if (pager.getCount() == 0) {
            return ResponseMessage.createBySuccess("暂无党员信息",pager);
        } else {
            return ResponseMessage.createBySuccess(pager);
        }
    }

    /**
     * 模糊查询 积极分子/发展对象/预备党员 信息
     * @param page
     * @param limit
     * @param keyWork
     * @return
     */
    @GetMapping(value = "getKeyWorkStudentInfoBy")
    public ResponseMessage getKeyWorkStudentInfoBy(int page,int limit, String keyWork, int tb_id){
        // 判断是查 积极分子/发展对象/预备党员 信息
        // 0为积极分子，1为发展对象，2为预备党员
        // 初始化
        Pager<Student> pager = null;
        if (tb_id == 0) {
            pager = studentService.findByKeyByPartyA(page,limit,keyWork);
        } else if (tb_id == 1){
            pager = studentService.findByKeyByDep(page,limit,keyWork);
        } else if (tb_id == 2) {
            pager = studentService.findByKeyByParty(page,limit,keyWork);
        }
        if (pager.getCount() == 0) {
            return ResponseMessage.createBySuccess("无该人员信息,请检查",pager);
        } else {
            return ResponseMessage.createBySuccess(pager);
        }
    }
}
