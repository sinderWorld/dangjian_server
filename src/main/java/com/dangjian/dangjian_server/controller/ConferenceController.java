package com.dangjian.dangjian_server.controller;

import com.dangjian.dangjian_server.common.ResponseMessage;
import com.dangjian.dangjian_server.pojo.Conference;
import com.dangjian.dangjian_server.pojo.Pager;
import com.dangjian.dangjian_server.pojo.Student;
import com.dangjian.dangjian_server.service.ConferenceService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * 支部会议管理
 */
@Slf4j
@RestController
@RequestMapping(value = "conference",produces = "application/json; charset=utf-8")
public class ConferenceController {
    @Autowired
    private ConferenceService conferenceService;

    /**
     * 分页查询支部会议信息
     * @param page
     * @param limit
     * @return
     */
    @GetMapping(value = "getPageConferenceInfo")
    public ResponseMessage getPageConferenceInfo(int page, int limit) {
        Pager<Conference> pager = conferenceService.findByPager(page,limit);

        System.out.println(pager);

        if (pager.getCount() == 0) {
            return ResponseMessage.createBySuccess("暂无会议信息",pager);
        } else {
            return ResponseMessage.createBySuccess(pager);
        }
    }

    /**
     * 模糊查询
     * @param page
     * @param limit
     * @param keyWork
     * @return
     */
    @GetMapping(value = "getKeyWorkConferenceInfo")
    public ResponseMessage getKeyWorkConferenceInfo(int page,int limit, String keyWork){
        Pager<Conference> pager = conferenceService.findByKey(page,limit,keyWork);
        if (pager.getCount() == 0) {
            return ResponseMessage.createBySuccess("无该会议信息,请检查",pager);
        } else {
            return ResponseMessage.createBySuccess(pager);
        }
    }

    /**
     * 删除一条记录
     * @param con_id
     * @return
     */
    @PostMapping(value = "delByIdCon")
    public ResponseMessage delByIdCon(int con_id){
        int code = conferenceService.deleteByPrimaryKey(con_id);
        return ResponseMessage.createBySuccess(code);
    }

    /**
     * 批量删除
     * @param array
     * @return
     */
    @PostMapping(value = "delByBatchCon")
    public ResponseMessage delByBatchCon(int[] array) {
        int code = conferenceService.deleteByBatchCon(array);
        return ResponseMessage.createBySuccess(code);
    }

    /**
     * 查看会议信息
     * @param con_id
     * @return
     */
    @GetMapping(value = "getByIdCon")
    public ResponseMessage getByIdCon(int con_id) {
        Conference conference = conferenceService.selectByPrimaryKey(con_id);
        System.out.println("会议信息：" + conference);
        if (conference == null) {
            return ResponseMessage.createByErrorMessage("查无此会议信息");
        } else {
            return ResponseMessage.createBySuccess("查看会议信息",conference);
        }
    }

    /**
     * 更新记录
     * @param conference
     * @return
     */
    @PostMapping(value = "updateByIdCon")
    public ResponseMessage updateByIdStu(@RequestBody Conference conference){
        System.out.println("获取的conference信息：" + conference.toString());
        int code = conferenceService.updateByPrimaryKeySelective(conference);
        if (code == 0) {
            return ResponseMessage.createByErrorMessage("更新失败");
        } else {
            return ResponseMessage.createBySuccess("更新成功", code);
        }
    }

    /**
     * 添加新会议信息
     * @param conference
     * @return
     */
    @PostMapping(value = "addConferenceInfo")
    public ResponseMessage addConferenceInfo(@RequestBody Conference conference){
        DateFormat df = new SimpleDateFormat("yyyyMMdd");
        Calendar calendar = Calendar.getInstance();
        String createTime = df.format(calendar.getTime());
        // 封装创建时间
        conference.setCreateTime(createTime);
        // 还得封装admin_id进去

        System.out.println(conference.toString());
        int code = conferenceService.insertSelective(conference);
        return ResponseMessage.createBySuccess("新增成功",code);
    }
}
