package com.dangjian.dangjian_server.controller;

import com.dangjian.dangjian_server.common.ResponseMessage;
import com.dangjian.dangjian_server.pojo.Opinion;
import com.dangjian.dangjian_server.service.OpinionService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 意见收集
 */
@Slf4j
@RestController
@RequestMapping(value = "opinion",produces = "application/json; charset=utf-8")
public class OpinionController {
    @Autowired
    private OpinionService opinionService;

    @PostMapping("addOpinion")
    public ResponseMessage addOpinion(@RequestBody Opinion opinion){
        int code = opinionService.insertSelective(opinion);
        return ResponseMessage.createBySuccess("成功发表意见",code);
    }
}
