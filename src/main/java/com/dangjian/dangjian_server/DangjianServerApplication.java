package com.dangjian.dangjian_server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DangjianServerApplication {
    public static void main(String[] args) {
        SpringApplication.run(DangjianServerApplication.class, args);
    }

}
