package com.dangjian.dangjian_server.service.impl;

import com.dangjian.dangjian_server.dao.StudentDao;
import com.dangjian.dangjian_server.pojo.Pager;
import com.dangjian.dangjian_server.pojo.Student;
import com.dangjian.dangjian_server.service.StudentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 实现 学生操作接口
 */
@Slf4j
@Service
public class StudentServiceImpl implements StudentService {

    @Autowired
    private StudentDao studentDao;

    /**
     * 删除一条记录
     * @param stuId
     * @return
     */
    @Override
    public int deleteByPrimaryKeyStu(Integer stuId) {
        int code = studentDao.deleteByPrimaryKeyStu(stuId);
        return code;
    }

    /**
     * 批量删除
     * @param array
     * @return
     */
    @Override
    public int deleteByBatchStu(int[] array) {
        int code = studentDao.deleteByBatchStu(array);
        return code;
    }

    /**
     * 新增一条记录
     * @param record
     * @return
     */
    @Override
    public int insertSelective(Student record) {
        int code = studentDao.insertSelective(record);
        return code;
    }

    /**
     * 分页查询
     * @param page
     * @param size
     * @return
     */
    @Override
    public Pager<Student> findByPager(int page,int size) {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("page", (page-1)*size);
        params.put("size", size);
        Pager<Student> pager = new Pager<Student>();
        List<Student> list = studentDao.findByPager(params);
        System.out.println("service中：" + list);
        pager.setData(list);
        pager.setCount(studentDao.count());
        return pager;
    }

    /**
     * 查询汇总数据
     * @return
     */
    @Override
    public long count() {
        return 0;
    }

    /**
     * 模糊查询
     * @param page
     * @param size
     * @param keyWork
     * @return
     */
    @Override
    public Pager<Student> findByKey(int page, int size, String keyWork) {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("page", (page-1)*size);
        params.put("size", size);
        params.put("keyWork", keyWork);
        Pager<Student> pager = new Pager<Student>();
        List<Student> list = studentDao.findByKey(params);
        pager.setData(list);
        pager.setCount(studentDao.count());
        return pager;
    }

    /**
     * 选择条件查询
     * @param page
     * @param size
     * @param col
     * @param code
     * @return
     */
    @Override
    public Pager<Student> findByCondition(int page, int size,String col,String code) {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("page", (page-1)*size);
        params.put("size", size);
        params.put("col", col);
        params.put("code", code);
        Pager<Student> pager = new Pager<Student>();
        List<Student> list = studentDao.findByCondition(params);
        pager.setData(list);
        pager.setCount(studentDao.count());
        return pager;
    }

    /**
     * 查询一条记录
     * @param stuId
     * @return
     */
    @Override
    public Student selectByPrimaryKeyStu(Integer stuId) {
        Student student = studentDao.selectByPrimaryKeyStu(stuId);
        return student;
    }

    /**
     * 根据学号查询一条记录
     * @param stuNumber
     * @return
     */
    @Override
    public Student selectByStuNumber(String stuNumber) {
        Student student = studentDao.selectByStuNumber(stuNumber);
        return student;
    }

    /**
     * 更新一条记录
     * @param record
     * @return
     */
    @Override
    public int updateByPrimaryKeySelective(Student record) {
        int code = studentDao.updateByPrimaryKeySelective(record);
        if (code == 0) {
            return 0;
        } else {
            return 1;
        }
    }


    // 分页查询 积极分子
    @Override
    public Pager<Student> findByPagerByPartyA(int page, int size) {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("page", (page-1)*size);
        params.put("size", size);
        Pager<Student> pager = new Pager<Student>();
        List<Student> list = studentDao.findByPagerByPartyA(params);
        System.out.println("service中：" + list);
        pager.setData(list);
        pager.setCount(studentDao.countByPartyA());
        return pager;
    }
    // 分页查询 发展对象
    @Override
    public Pager<Student> findByPagerByDep(int page, int size) {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("page", (page-1)*size);
        params.put("size", size);
        Pager<Student> pager = new Pager<Student>();
        List<Student> list = studentDao.findByPagerByDep(params);
        System.out.println("service中：" + list);
        pager.setData(list);
        pager.setCount(studentDao.countByDep());
        return pager;
    }
    // 分页查询 预备党员
    @Override
    public Pager<Student> findByPagerByParty(int page, int size) {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("page", (page-1)*size);
        params.put("size", size);
        Pager<Student> pager = new Pager<Student>();
        List<Student> list = studentDao.findByPagerByParty(params);
        System.out.println("service中：" + list);
        pager.setData(list);
        pager.setCount(studentDao.countByParty());
        return pager;
    }
    // 模糊查询 积极分子
    @Override
    public Pager<Student> findByKeyByPartyA(int page, int size, String keyWork) {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("page", (page-1)*size);
        params.put("size", size);
        params.put("keyWork", keyWork);
        Pager<Student> pager = new Pager<Student>();
        List<Student> list = studentDao.findByKeyByPartyA(params);
        pager.setData(list);
        pager.setCount(studentDao.countByPartyA());
        return pager;
    }
    // 模糊查询 发展对象
    @Override
    public Pager<Student> findByKeyByDep(int page, int size, String keyWork) {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("page", (page-1)*size);
        params.put("size", size);
        params.put("keyWork", keyWork);
        Pager<Student> pager = new Pager<Student>();
        List<Student> list = studentDao.findByKeyByDep(params);
        pager.setData(list);
        pager.setCount(studentDao.countByDep());
        return pager;
    }
    // 模糊查询 预备党员
    @Override
    public Pager<Student> findByKeyByParty(int page, int size, String keyWork) {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("page", (page-1)*size);
        params.put("size", size);
        params.put("keyWork", keyWork);
        Pager<Student> pager = new Pager<Student>();
        List<Student> list = studentDao.findByKeyByParty(params);
        pager.setData(list);
        pager.setCount(studentDao.countByParty());
        return pager;
    }

    @Override
    public long countByPartyA() {
        return 0;
    }

    @Override
    public long countByDep() {
        return 0;
    }

    @Override
    public long countByParty() {
        return 0;
    }
}
