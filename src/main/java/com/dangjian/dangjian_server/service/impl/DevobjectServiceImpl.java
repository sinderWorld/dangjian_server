package com.dangjian.dangjian_server.service.impl;

import com.dangjian.dangjian_server.dao.DevobjectDao;
import com.dangjian.dangjian_server.dao.StudentDao;
import com.dangjian.dangjian_server.pojo.Activist;
import com.dangjian.dangjian_server.pojo.Devobject;
import com.dangjian.dangjian_server.pojo.Pager;
import com.dangjian.dangjian_server.service.DevobjectService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Service
public class DevobjectServiceImpl implements DevobjectService {
    @Autowired
    private DevobjectDao devobjectDao;

    @Override
    public Pager<Devobject> findByPager(int page, int size, int sta_id) {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("page", (page-1)*size);
        params.put("size", size);
        params.put("sta_id", sta_id);
        Pager<Devobject> pager = new Pager<Devobject>();
        List<Devobject> list = devobjectDao.findByPager(params);
        System.out.println("service中：" + list);
        pager.setData(list);
        pager.setCount(devobjectDao.count(sta_id));
        return pager;
    }

    @Override
    public long count(int sta_id) {
        return 0;
    }

    @Override
    public int deleteByPrimaryKey(String stuNumber) {
        int code = devobjectDao.deleteByPrimaryKey(stuNumber);
        return code;
    }

    /**
     * 添加记录 学生添加
     * @param record
     * @return
     */
    @Override
    public int updateByPrimaryKeySelective(Devobject record) {
        int code = devobjectDao.updateByPrimaryKeySelective(record);
        return code;
    }
}
