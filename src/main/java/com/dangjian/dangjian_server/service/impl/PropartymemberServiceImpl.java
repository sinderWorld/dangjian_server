package com.dangjian.dangjian_server.service.impl;

import com.dangjian.dangjian_server.dao.ActivistDao;
import com.dangjian.dangjian_server.dao.PropartymemberDao;
import com.dangjian.dangjian_server.pojo.Activist;
import com.dangjian.dangjian_server.pojo.Pager;
import com.dangjian.dangjian_server.pojo.Propartymember;
import com.dangjian.dangjian_server.service.PropartymemberService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Service
public class PropartymemberServiceImpl implements PropartymemberService {
    @Autowired
    private PropartymemberDao propartymemberDao;

    @Override
    public Pager<Propartymember> findByPager(int page, int size, int sta_id) {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("page", (page-1)*size);
        params.put("size", size);
        params.put("sta_id", sta_id);
        Pager<Propartymember> pager = new Pager<Propartymember>();
        List<Propartymember> list = propartymemberDao.findByPager(params);
        System.out.println("service中：" + list);
        pager.setData(list);
        pager.setCount(propartymemberDao.count(sta_id));
        return pager;
    }

    @Override
    public long count(int sta_id) {
        return 0;
    }

    @Override
    public int deleteByPrimaryKey(String stuNumber) {
        int code = propartymemberDao.deleteByPrimaryKey(stuNumber);
        return code;
    }

    /**
     * 更新记录 学生填写
     * @param record
     * @return
     */
    @Override
    public int updateByPrimaryKeySelective(Propartymember record) {
        int code = propartymemberDao.updateByPrimaryKeySelective(record);
        return code;
    }
}
