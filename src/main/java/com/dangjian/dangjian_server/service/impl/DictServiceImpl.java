package com.dangjian.dangjian_server.service.impl;

import com.dangjian.dangjian_server.dao.DictDao;
import com.dangjian.dangjian_server.pojo.Dict;
import com.dangjian.dangjian_server.service.DictService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 实现 字典操作接口
 */
@Slf4j
@Service
public class DictServiceImpl implements DictService {

    @Autowired
    private DictDao dictDao;

    /**
     * 根据belong值查询所属同类型的字典值
     * @param belongValue
     * @return
     */
    @Override
    public List<Dict> getByBelong(String belongValue) {
        List<Dict> dicts = dictDao.getByBelong(belongValue);
        return dicts;
    }
}
