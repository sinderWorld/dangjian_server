package com.dangjian.dangjian_server.service.impl;

import com.dangjian.dangjian_server.dao.AdminDao;
import com.dangjian.dangjian_server.pojo.Admin;
import com.dangjian.dangjian_server.service.AdminService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class AdminServiceImpl implements AdminService {
    @Autowired
    private AdminDao adminDao;

    /**
     * 登录
     * @param account
     * @param password
     * @return
     */
    @Override
    public Admin login(String account, String password) {
        Admin admin = adminDao.login(account,password);
        return admin;
    }

    /**
     * 更新信息
     * @param record
     * @return
     */
    @Override
    public int updateByPrimaryKeySelective(Admin record) {
        int code = adminDao.updateByPrimaryKeySelective(record);
        return code;
    }
}
