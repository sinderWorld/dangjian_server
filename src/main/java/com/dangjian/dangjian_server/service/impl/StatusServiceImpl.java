package com.dangjian.dangjian_server.service.impl;

import com.dangjian.dangjian_server.dao.StatusDao;
import com.dangjian.dangjian_server.pojo.Announcement;
import com.dangjian.dangjian_server.pojo.Pager;
import com.dangjian.dangjian_server.pojo.Status;
import com.dangjian.dangjian_server.service.StatusService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 实现 信息发布收集接口
 */
@Service
@Slf4j
public class StatusServiceImpl implements StatusService {
    @Autowired
    private StatusDao statusDao;

    /**
     * 删除一条记录
     * @param sta_id
     * @return
     */
    @Override
    public int deleteByPrimaryKey(Integer sta_id) {
        int code = statusDao.deleteByPrimaryKey(sta_id);
        return code;
    }

    /**
     * 发布 添加 新的信息收集
     * @param record
     * @return
     */
    @Override
    public int insertSelective(Status record) {
        int code = statusDao.insertSelective(record);
        System.out.println("返回的值：" + code);
        if (code == 0) {
            return 0;
        } else {
            // 成功
            return 1;
        }
    }

    /**
     * 查询一条信息
     * @param sta_id
     * @return
     */
    @Override
    public Status selectByPrimaryKey(Integer sta_id) {
        Status status = statusDao.selectByPrimaryKey(sta_id);
        return status;
    }

    /**
     * 更新一条记录
     * @param record
     * @return
     */
    @Override
    public int updateByPrimaryKeySelective(Status record) {
        int code = statusDao.updateByPrimaryKeySelective(record);
        if (code == 0) {
            return 0;
        } else {
            return 1;
        }
    }

    /**
     * 分页查询
     * @param page
     * @param size
     * @return
     */
    @Override
    public Pager<Status> findByPager(int page, int size) {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("page", (page-1)*size);
        params.put("size", size);
        Pager<Status> pager = new Pager<Status>();
        List<Status> list = statusDao.findByPager(params);
        System.out.println("service中：" + list);
        pager.setData(list);
        pager.setCount(statusDao.count());
        return pager;
    }

    @Override
    public long count() {
        return 0;
    }

    /**
     * 模糊查询
     * @param page
     * @param size
     * @param keyWork
     * @return
     */
    @Override
    public Pager<Status> findByKey(int page, int size, String keyWork) {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("page", (page-1)*size);
        params.put("size", size);
        params.put("keyWork", keyWork);
        Pager<Status> pager = new Pager<Status>();
        List<Status> list = statusDao.findByKey(params);
        pager.setData(list);
        pager.setCount(statusDao.count());
        return pager;
    }
}
