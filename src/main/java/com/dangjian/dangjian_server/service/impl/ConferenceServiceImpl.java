package com.dangjian.dangjian_server.service.impl;

import com.dangjian.dangjian_server.dao.ConferenceDao;
import com.dangjian.dangjian_server.pojo.Conference;
import com.dangjian.dangjian_server.pojo.Pager;
import com.dangjian.dangjian_server.pojo.Student;
import com.dangjian.dangjian_server.service.ConferenceService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 实现 支部会议操作接口
 */
@Slf4j
@Service
public class ConferenceServiceImpl implements ConferenceService{
    @Autowired
    private ConferenceDao conferenceDao;

    /**
     * 删除一条记录
     * @param con_id
     * @return
     */
    @Override
    public int deleteByPrimaryKey(Integer con_id) {
        int code = conferenceDao.deleteByPrimaryKey(con_id);
        return code;
    }

    /**
     * 批量删除
     * @param array
     * @return
     */
    @Override
    public int deleteByBatchCon(int[] array) {
        int code = conferenceDao.deleteByBatchCon(array);
        return code;
    }


    /**
     * 按需添加会议
     * @param record
     * @return
     */
    @Override
    public int insertSelective(Conference record) {
        int code = conferenceDao.insertSelective(record);
        if (code == 0) {
            return 0;
        } else {
            return 1;
        }
    }

    /**
     * 查询一条记录
     * @param con_id
     * @return
     */
    @Override
    public Conference selectByPrimaryKey(Integer con_id) {
        Conference conference = conferenceDao.selectByPrimaryKey(con_id);
        return conference;
    }
    /**
     * 更新一条记录
     * @param record
     * @return
     */
    @Override
    public int updateByPrimaryKeySelective(Conference record) {
        int code = conferenceDao.updateByPrimaryKeySelective(record);
        if (code == 0) {
            return 0;
        } else {
            return 1;
        }
    }
    /**
     * 分页查询
     * @param page
     * @param size
     * @return
     */
    @Override
    public Pager<Conference> findByPager(int page,int size) {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("page", (page-1)*size);
        params.put("size", size);
        Pager<Conference> pager = new Pager<Conference>();
        List<Conference> list = conferenceDao.findByPager(params);
        System.out.println("service中：" + list);
        pager.setData(list);
        pager.setCount(conferenceDao.count());
        return pager;
    }

    @Override
    public long count() {
        return 0;
    }

    /**
     * 模糊查询
     * @param page
     * @param size
     * @param keyWork
     * @return
     */
    @Override
    public Pager<Conference> findByKey(int page, int size, String keyWork) {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("page", (page-1)*size);
        params.put("size", size);
        params.put("keyWork", keyWork);
        Pager<Conference> pager = new Pager<Conference>();
        List<Conference> list = conferenceDao.findByKey(params);
        pager.setData(list);
        pager.setCount(conferenceDao.count());
        return pager;
    }
}
