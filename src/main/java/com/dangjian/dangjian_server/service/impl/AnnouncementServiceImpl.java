package com.dangjian.dangjian_server.service.impl;

import com.dangjian.dangjian_server.dao.AnnouncementDao;
import com.dangjian.dangjian_server.pojo.Announcement;
import com.dangjian.dangjian_server.pojo.Conference;
import com.dangjian.dangjian_server.pojo.Pager;
import com.dangjian.dangjian_server.service.AnnouncementService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 实现 支部公告操作接口
 */
@Slf4j
@Service
public class AnnouncementServiceImpl implements AnnouncementService {
    @Autowired
    private AnnouncementDao announcementDao;

    /**
     * 删除一条记录
     * @param ann_id
     * @return
     */
    @Override
    public int deleteByPrimaryKey(Integer ann_id) {
        int code = announcementDao.deleteByPrimaryKey(ann_id);
        return code;
    }

    /**
     * 批量删除
     * @param array
     * @return
     */
    @Override
    public int deleteByBatchAnn(int[] array) {
        int code = announcementDao.deleteByBatchAnn(array);
        return code;
    }

    /**
     * 按需添加公告
     * @param record
     * @return
     */
    @Override
    public int insertSelective(Announcement record) {
        int code = announcementDao.insertSelective(record);
        if (code == 0) {
            return 0;
        } else {
            return 1;
        }
    }

    /**
     * 查询一条记录
     * @param ann_id
     * @return
     */
    @Override
    public Announcement selectByPrimaryKey(Integer ann_id) {
        Announcement announcement = announcementDao.selectByPrimaryKey(ann_id);
        return announcement;
    }

    /**
     * 更新一条记录
     * @param record
     * @return
     */
    @Override
    public int updateByPrimaryKeySelective(Announcement record) {
        int code = announcementDao.updateByPrimaryKeySelective(record);
        if (code == 0) {
            return 0;
        } else {
            return 1;
        }
    }

    /**
     * 分页查询
     * @param page
     * @param size
     * @return
     */
    @Override
    public Pager<Announcement> findByPager(int page, int size) {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("page", (page-1)*size);
        params.put("size", size);
        Pager<Announcement> pager = new Pager<Announcement>();
        List<Announcement> list = announcementDao.findByPager(params);
        System.out.println("service中：" + list);
        pager.setData(list);
        pager.setCount(announcementDao.count());
        return pager;
    }

    @Override
    public long count() {
        return 0;
    }

    /**
     * 模糊查询
     * @param page
     * @param size
     * @param keyWork
     * @return
     */
    @Override
    public Pager<Announcement> findByKey(int page, int size, String keyWork) {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("page", (page-1)*size);
        params.put("size", size);
        params.put("keyWork", keyWork);
        Pager<Announcement> pager = new Pager<Announcement>();
        List<Announcement> list = announcementDao.findByKey(params);
        pager.setData(list);
        pager.setCount(announcementDao.count());
        return pager;
    }
}
