package com.dangjian.dangjian_server.service.impl;

import com.dangjian.dangjian_server.dao.ActivistDao;
import com.dangjian.dangjian_server.pojo.Activist;
import com.dangjian.dangjian_server.pojo.Pager;
import com.dangjian.dangjian_server.pojo.Student;
import com.dangjian.dangjian_server.service.ActivistService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Service
public class ActivistServiceImpl implements ActivistService {
    @Autowired
    private ActivistDao activistDao;

    @Override
    public Pager<Activist> findByPager(int page, int size,int sta_id) {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("page", (page-1)*size);
        params.put("size", size);
        params.put("sta_id", sta_id);
        Pager<Activist> pager = new Pager<Activist>();
        List<Activist> list = activistDao.findByPager(params);
        System.out.println("service中：" + list);
        pager.setData(list);
        pager.setCount(activistDao.count(sta_id));
        return pager;
    }

    @Override
    public long count(int sta_id) {
        return 0;
    }

    @Override
    public int deleteByPrimaryKey(String stuNumber) {
        int code = activistDao.deleteByPrimaryKey(stuNumber);
        return code;
    }

    /**
     * 更新信息
     * @param record
     * @return
     */
    @Override
    public int updateByPrimaryKeySelective(Activist record) {
        int code = activistDao.updateByPrimaryKeySelective(record);
        return code;
    }
}
