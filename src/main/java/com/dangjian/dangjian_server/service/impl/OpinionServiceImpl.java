package com.dangjian.dangjian_server.service.impl;

import com.dangjian.dangjian_server.dao.OpinionDao;
import com.dangjian.dangjian_server.pojo.Announcement;
import com.dangjian.dangjian_server.pojo.Opinion;
import com.dangjian.dangjian_server.pojo.Pager;
import com.dangjian.dangjian_server.service.OpinionService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 实现 支部意见操作接口
 */
@Slf4j
@Service
public class OpinionServiceImpl implements OpinionService {
    @Autowired
    private OpinionDao opinionDao;

    /**
     * 查询记录
     * @param ann_id
     * @return
     */
    @Override
    public List<Opinion> findByAnnId(Integer ann_id) {
        List<Opinion> opinionList = opinionDao.findByAnnId(ann_id);
        return opinionList;
    }

    /**
     * 删除一条记录
     * @param ann_id
     * @return
     */
    @Override
    public int deleteByPrimaryKey(Integer ann_id) {
        int code = opinionDao.deleteByPrimaryKey(ann_id);
        return code;
    }

    /**
     * 批量删除
     * @param array
     * @return
     */
    @Override
    public int deleteByBatchOpi(int[] array) {
        int code = opinionDao.deleteByBatchOpi(array);
        return code;
    }

    /**
     * 添加意见
     * @param record
     * @return
     */
    @Override
    public int insertSelective(Opinion record) {
        int code = opinionDao.insertSelective(record);
        if (code == 0) {
            return 0;
        } else {
            return 1;
        }
    }

    /**
     * 分页查询
     * @param page
     * @param size
     * @return
     */
    @Override
    public Pager<Opinion> findByPager(int page, int size) {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("page", (page-1)*size);
        params.put("size", size);
        Pager<Opinion> pager = new Pager<Opinion>();
        List<Opinion> list = opinionDao.findByPager(params);
        System.out.println("service中：" + list);
        pager.setData(list);
        pager.setCount(opinionDao.count());
        return pager;
    }

    @Override
    public long count() {
        return 0;
    }

    /**
     * 模糊查询
     * @param page
     * @param size
     * @param keyWork
     * @return
     */
    @Override
    public Pager<Opinion> findByKey(int page, int size, String keyWork) {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("page", (page-1)*size);
        params.put("size", size);
        params.put("keyWork", keyWork);
        Pager<Opinion> pager = new Pager<Opinion>();
        List<Opinion> list = opinionDao.findByKey(params);
        pager.setData(list);
        pager.setCount(opinionDao.count());
        return pager;
    }
}
