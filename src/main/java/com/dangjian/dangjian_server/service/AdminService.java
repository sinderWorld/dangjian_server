package com.dangjian.dangjian_server.service;

import com.dangjian.dangjian_server.pojo.Admin;

// 管理员操作
public interface AdminService {
    // 登录接口
    Admin login(String admAccount, String admPassword);

    // 更新信息 修改密码
    int updateByPrimaryKeySelective(Admin record);
}
