package com.dangjian.dangjian_server.service;

import com.dangjian.dangjian_server.pojo.Conference;
import com.dangjian.dangjian_server.pojo.Pager;
import com.dangjian.dangjian_server.pojo.Student;

import java.util.List;
import java.util.Map;

/**
 * 支部会议 逻辑接口
 */
public interface ConferenceService {
    // 根据id删除一条记录
    int deleteByPrimaryKey(Integer con_id);
    // 根据传入的id数组批量删除
    int deleteByBatchCon(int[] array);
    // 添加一条新纪录
    int insertSelective(Conference record);
    // 根据id查询一条记录
    Conference selectByPrimaryKey(Integer con_id);
    // 根据id更新记录
    int updateByPrimaryKeySelective(Conference record);
    // 分页查询
    Pager<Conference> findByPager(int page, int size);
    // 汇总数据
    long count();
    // 模糊查询
    Pager<Conference> findByKey(int page, int size,String keyWork);
}
