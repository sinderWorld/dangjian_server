package com.dangjian.dangjian_server.service;

import com.dangjian.dangjian_server.pojo.Announcement;
import com.dangjian.dangjian_server.pojo.Opinion;
import com.dangjian.dangjian_server.pojo.Pager;

import java.util.List;

/**
 * 意见 逻辑接口
 */
public interface OpinionService {
    // 根据ann_id查询意见信息
    List<Opinion> findByAnnId(Integer ann_id);
    // 根据ann_id删除一条记录
    int deleteByPrimaryKey(Integer ann_id);
    int deleteByBatchOpi(int[] array);
    // 添加一条新纪录
    int insertSelective(Opinion record);


    // 分页查询
    Pager<Opinion> findByPager(int page, int size);
    // 汇总数据
    long count();
    // 模糊查询
    Pager<Opinion> findByKey(int page, int size,String keyWork);
}
