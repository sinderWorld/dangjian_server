package com.dangjian.dangjian_server.service;

import com.dangjian.dangjian_server.pojo.Announcement;
import com.dangjian.dangjian_server.pojo.Conference;
import com.dangjian.dangjian_server.pojo.Pager;

/**
 * 公告 逻辑接口
 */
public interface AnnouncementService {
    // 根据id删除一条记录
    int deleteByPrimaryKey(Integer ann_id);
    // 根据传入的id数组批量删除
    int deleteByBatchAnn(int[] array);
    // 添加一条新纪录
    int insertSelective(Announcement record);
    // 根据id查询一条记录
    Announcement selectByPrimaryKey(Integer ann_id);
    // 根据id更新记录
    int updateByPrimaryKeySelective(Announcement record);
    // 分页查询
    Pager<Announcement> findByPager(int page, int size);
    // 汇总数据
    long count();
    // 模糊查询
    Pager<Announcement> findByKey(int page, int size,String keyWork);
}
