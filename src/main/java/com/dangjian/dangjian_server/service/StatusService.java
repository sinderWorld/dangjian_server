package com.dangjian.dangjian_server.service;

import com.dangjian.dangjian_server.pojo.Announcement;
import com.dangjian.dangjian_server.pojo.Pager;
import com.dangjian.dangjian_server.pojo.Status;

/**
 * 信息收集模板 逻辑接口
 */
public interface StatusService {
    // 根据id删除一条记录
    int deleteByPrimaryKey(Integer sta_id);
    // 发布信息收集
    int insertSelective(Status record);
    // 根据id查询一条记录
    Status selectByPrimaryKey(Integer sta_id);
    // 根据id更新记录
    int updateByPrimaryKeySelective(Status record);
    // 分页查询
    Pager<Status> findByPager(int page, int size);
    // 汇总数据
    long count();
    // 模糊查询
    Pager<Status> findByKey(int page, int size,String keyWork);
}
