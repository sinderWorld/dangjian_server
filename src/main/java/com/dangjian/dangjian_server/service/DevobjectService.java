package com.dangjian.dangjian_server.service;

import com.dangjian.dangjian_server.pojo.Activist;
import com.dangjian.dangjian_server.pojo.Devobject;
import com.dangjian.dangjian_server.pojo.Pager;

public interface DevobjectService {
    // 分页查询
    Pager<Devobject> findByPager(int page, int size, int sta_id);

    // 汇总数据
    long count(int sta_id);

    // 移除一条记录
    int deleteByPrimaryKey(String stuNumber);

    // 更新记录 学生添加
    int updateByPrimaryKeySelective(Devobject record);
}
