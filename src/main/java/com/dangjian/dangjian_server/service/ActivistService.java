package com.dangjian.dangjian_server.service;

import com.dangjian.dangjian_server.pojo.Activist;
import com.dangjian.dangjian_server.pojo.Pager;
import com.dangjian.dangjian_server.pojo.Student;

public interface ActivistService {
    // 分页查询
    Pager<Activist> findByPager(int page, int size,int sta_id);

    // 汇总数据
    long count(int sta_id);

    // 移除一条记录
    int deleteByPrimaryKey(String stuNumber);

    // 更新信息 学生填写
    int updateByPrimaryKeySelective(Activist record);
}
