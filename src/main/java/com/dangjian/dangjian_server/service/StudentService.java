package com.dangjian.dangjian_server.service;

import com.dangjian.dangjian_server.pojo.Pager;
import com.dangjian.dangjian_server.pojo.Student;

import java.util.List;
import java.util.Map;

/**
 * 学生操作 逻辑接口
 */
public interface StudentService {
    // 删除
    int deleteByPrimaryKeyStu(Integer stuId);
    // 批量删除
    int deleteByBatchStu(int[] array);
    // 新增一条记录 只给有值的字段赋值
    int insertSelective(Student record);
    // 分页查询
    Pager<Student> findByPager(int page, int size);

    // 汇总数据
    long count();

    // 模糊查询
    Pager<Student> findByKey(int page, int size, String keyWork);

    Pager<Student> findByCondition(int page, int size,String col,String code);

    // 查询一条记录
    Student selectByPrimaryKeyStu(Integer stuId);
    Student selectByStuNumber(String stuNumber);

    int updateByPrimaryKeySelective(Student record);


    // 分页查询 积极分子
    Pager<Student> findByPagerByPartyA(int page, int size);
    // 分页查询 发展对象
    Pager<Student> findByPagerByDep(int page, int size);
    // 分页查询 预备党员
    Pager<Student> findByPagerByParty(int page, int size);
    // 模糊查询 积极分子
    Pager<Student> findByKeyByPartyA(int page, int size, String keyWork);
    // 模糊查询 发展对象
    Pager<Student> findByKeyByDep(int page, int size, String keyWork);
    // 模糊查询 预备党员
    Pager<Student> findByKeyByParty(int page, int size, String keyWork);
    // 汇总数据
    long countByPartyA();
    long countByDep();
    long countByParty();
}
