package com.dangjian.dangjian_server.service;

import com.dangjian.dangjian_server.pojo.Dict;

import java.util.List;

/**
 * 字典操作
 */
public interface DictService {
    List<Dict> getByBelong(String belongValue);
}
