package com.dangjian.dangjian_server.dao;

import com.dangjian.dangjian_server.pojo.Activist;
import com.dangjian.dangjian_server.pojo.Student;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * 固定信息收集模板表_积极分子信息收集表
 */
@Mapper
public interface ActivistDao {
    // 校验是否已经存在该学生
    Activist checkIsStu(int sta_id,String stuNumber);
    // 移除一条记录
    int deleteByPrimaryKey(String stuNumber);
    // 分页查询
    List<Activist> findByPager(Map<String, Object> params);
    // 汇总数据
    long count(int sta_id);

    int insert(Activist record);
    // 添加学生
    int insertSelective(Activist record);

    Activist selectByPrimaryKey(Integer actId);
    // 更新信息 学生填写
    int updateByPrimaryKeySelective(Activist record);

    int updateByPrimaryKey(Activist record);

    Activist selectInfoByNumber(int sta_id,String stuNumber);
}