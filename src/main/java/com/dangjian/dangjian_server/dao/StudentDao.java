package com.dangjian.dangjian_server.dao;

import com.dangjian.dangjian_server.pojo.Student;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;
@Mapper
public interface StudentDao {
    // 根据id删除一条记录
    int deleteByPrimaryKeyStu(Integer stuId);
    // 根据传入的id数组批量删除
    int deleteByBatchStu(int[] array);

    int insert(Student record);
    // 新增一条记录 只给有值的字段赋值
    int insertSelective(Student record);
    // 根据id查询一条记录
    Student selectByPrimaryKeyStu(Integer stuId);
    // 根据openId查询信息
    Student selectByOpenId(String open_id);
    // 根据学号查询一条记录 校验是否存在该学生
    Student selectByStuNumber(String stuNumber);
    // 根据id更新记录
    int updateByPrimaryKeySelective(Student record);

    int updateByPrimaryKey(Student record);

    // 分页查询
    List<Student> findByPager(Map<String, Object> params);
    // 汇总数据
    long count();
    // 模糊查询
    List<Student> findByKey(Map<String, Object> params);
    // 选择条件查询
    List<Student> findByCondition(Map<String, Object> params);

    // 分页查询 积极分子
    List<Student> findByPagerByPartyA(Map<String, Object> params);
    // 分页查询 发展对象
    List<Student> findByPagerByDep(Map<String, Object> params);
    // 分页查询 预备党员
    List<Student> findByPagerByParty(Map<String, Object> params);
    // 模糊查询 积极分子
    List<Student> findByKeyByPartyA(Map<String, Object> params);
    // 模糊查询 发展对象
    List<Student> findByKeyByDep(Map<String, Object> params);
    // 模糊查询 预备党员
    List<Student> findByKeyByParty(Map<String, Object> params);
    // 汇总数据
    long countByPartyA();
    long countByDep();
    long countByParty();
}