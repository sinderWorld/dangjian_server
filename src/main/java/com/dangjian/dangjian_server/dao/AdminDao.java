package com.dangjian.dangjian_server.dao;

import com.dangjian.dangjian_server.pojo.Admin;
import org.apache.ibatis.annotations.Mapper;

/**
 * 后台系统操作员操作
 */
@Mapper
public interface AdminDao {
    // 登录接口
    Admin login(String account,String password);

    int deleteByPrimaryKey(Integer admId);

    int insert(Admin record);

    int insertSelective(Admin record);

    Admin selectByPrimaryKey(Integer admId);

    // 更新信息 修改密码
    int updateByPrimaryKeySelective(Admin record);

    int updateByPrimaryKey(Admin record);
}