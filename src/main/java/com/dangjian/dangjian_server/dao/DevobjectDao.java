package com.dangjian.dangjian_server.dao;

import com.dangjian.dangjian_server.pojo.Activist;
import com.dangjian.dangjian_server.pojo.Devobject;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * 固定信息收集模板表_发展对象信息收集表
 */
@Mapper
public interface DevobjectDao {
    // 校验是否已经存在该学生
    Devobject checkIsStu(int sta_id, String stuNumber);
    // 分页查询
    List<Devobject> findByPager(Map<String, Object> params);
    // 汇总数据
    long count(int sta_id);
    // 移除 改成根据学号删除
    int deleteByPrimaryKey(String stuNumber);

    int insert(Devobject record);
    // 添加学生
    int insertSelective(Devobject record);

    Devobject selectByPrimaryKey(Integer devId);
    // 更新记录 学生添加
    int updateByPrimaryKeySelective(Devobject record);

    int updateByPrimaryKey(Devobject record);

    Devobject selectInfoByNumber(int sta_id,String stuNumber);
}