package com.dangjian.dangjian_server.dao;

import com.dangjian.dangjian_server.pojo.Announcement;
import com.dangjian.dangjian_server.pojo.Opinion;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * 公告意见收集
 */
@Mapper
public interface OpinionDao {
    // 根据ann_id查询意见信息
    List<Opinion> findByAnnId(Integer ann_id);
    // 分页查询
    List<Opinion> findByPager(Map<String, Object> params);
    // 汇总数据
    long count();
    // 模糊查询
    List<Opinion> findByKey(Map<String, Object> params);
    // 根据ann_id删除一条记录
    int deleteByPrimaryKey(Integer ann_id);
    int deleteByBatchOpi(int[] array);
    // 添加一条新纪录
    int insertSelective(Opinion record);

    Opinion selectByPrimaryKey(Integer opiId);
    int insert(Opinion record);
    int updateByPrimaryKeySelective(Opinion record);

    int updateByPrimaryKey(Opinion record);
}