package com.dangjian.dangjian_server.dao;

import com.dangjian.dangjian_server.pojo.Dict;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 字典项应用
 */
@Mapper
public interface DictDao {
    int deleteByPrimaryKey(Integer dictId);

    int insert(Dict record);

    int insertSelective(Dict record);

    Dict selectByPrimaryKey(Integer dictId);

    int updateByPrimaryKeySelective(Dict record);

    int updateByPrimaryKey(Dict record);

    List<Dict> getByBelong(String belongValue);
}