package com.dangjian.dangjian_server.dao;

import com.dangjian.dangjian_server.pojo.Activist;
import com.dangjian.dangjian_server.pojo.Devobject;
import com.dangjian.dangjian_server.pojo.Status;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 校验之前先校验是否存在该学生
 */
@Component
@Mapper
public interface TemplateDao {

    // 普通入党申请获取是否有积极分子信息可填
    @Select("SELECT b.* FROM `tb_activist` a,tb_status b where a.stuNumber = #{stuNumber} and a.sta_id = b.sta_id and b.`status` = 1")
    List<Status> getActivist(String stuNumber);

    // 积极分子获取是否有发展对象信息可填
    @Select("SELECT b.* FROM `tb_devobject` a,tb_status b where a.stuNumber = #{stuNumber} and a.sta_id = b.sta_id and b.`status` = 1")
    List<Status> getDevobject(String stuNumber);

    // 发展对象获取是否有预备党员信息可填
    @Select("SELECT b.* FROM `tb_propartymember` a,tb_status b where a.stuNumber = #{stuNumber} and a.sta_id = b.sta_id and b.`status` = 1")
    List<Status> getPropartymember(String stuNumber);
}
