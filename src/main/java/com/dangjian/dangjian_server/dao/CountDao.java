package com.dangjian.dangjian_server.dao;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Component;

/**
 * 党建系统
 * 数据汇总 分析
 */
@Component
@Mapper  //@Mapper=@Repository+@MapperScane
public interface CountDao {
    /**
     * 汇总积极分子数据
     * 入党积极分子时间 partyActivistTime not null
     * 成为发展对象时间 depTargetTime null
     * 成为预备党员时间 partyTime null
     * 转正时间 turnPositiveTime null
     * @return
     */
    @Select("SELECT count(stu_id) FROM `tb_student` \n" +
            "where \n" +
            "(partyAppliTime is not null and trim(partyAppliTime) !='')\n" +
            "and (partyActivistTime is not null and trim(partyActivistTime) !='')\n" +
            "and (depTargetTime is null or trim(depTargetTime) ='')\n" +
            "and (partyTime is null or trim(partyTime) ='')\n" +
            "and (turnPositiveTime is null or trim(turnPositiveTime) ='')")
    int activistCount();

    /**
     * 汇总发展对象数据
     * 入党积极分子时间 partyActivistTime not null
     * 成为发展对象时间 depTargetTime not null
     * 成为预备党员时间 partyTime null
     * 转正时间 turnPositiveTime null
     * @return
     */
    @Select("SELECT count(stu_id) FROM `tb_student` \n" +
            "where \n" +
            "(partyAppliTime is not null and trim(partyAppliTime) !='')\n" +
            "and (partyActivistTime is not null and trim(partyActivistTime) !='')\n" +
            "and (depTargetTime is not null and trim(depTargetTime) !='')\n" +
            "and (partyTime is null or trim(partyTime) ='')\n" +
            "and (turnPositiveTime is null or trim(turnPositiveTime) ='')")
    int depTargetCount();

    /**
     * 汇总预备数据
     * 入党积极分子时间 partyActivistTime not null
     * 成为发展对象时间 depTargetTime not null
     * 成为预备党员时间 partyTime not null
     * 转正时间 turnPositiveTime null
     * @return
     */
    @Select("SELECT count(stu_id) FROM `tb_student` \n" +
            "where \n" +
            "(partyAppliTime is not null and trim(partyAppliTime) !='')\n" +
            "and (partyActivistTime is not null and trim(partyActivistTime) !='')\n" +
            "and (depTargetTime is not null and trim(depTargetTime) !='')\n" +
            "and (partyTime is not null and trim(partyTime) !='')\n" +
            "and (turnPositiveTime is null or trim(turnPositiveTime) ='')")
    int partyCount();

    /**
     * 汇总正式党员数据
     * 入党积极分子时间 partyActivistTime not null
     * 成为发展对象时间 depTargetTime not null
     * 成为预备党员时间 partyTime not null
     * 转正时间 turnPositiveTime not null
     * @return
     */
    @Select("SELECT count(stu_id) FROM `tb_student` \n" +
            "where \n" +
            "(partyAppliTime is not null and trim(partyAppliTime) !='')\n" +
            "and (partyActivistTime is not null and trim(partyActivistTime) !='')\n" +
            "and (depTargetTime is not null and trim(depTargetTime) !='')\n" +
            "and (partyTime is not null and trim(partyTime) !='')\n" +
            "and (turnPositiveTime is not null and trim(turnPositiveTime) !='')")
    int turnPositiveCount();
}
