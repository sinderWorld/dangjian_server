package com.dangjian.dangjian_server.dao;

import com.dangjian.dangjian_server.pojo.Announcement;
import com.dangjian.dangjian_server.pojo.Conference;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * 支部公告信息操作
 */
@Mapper
public interface AnnouncementDao {
    // 根据id删除一条记录
    int deleteByPrimaryKey(Integer ann_id);
    // 根据传入的id数组批量删除
    int deleteByBatchAnn(int[] array);
    // 添加一条新纪录
    int insertSelective(Announcement record);
    // 根据id查询一条记录
    Announcement selectByPrimaryKey(Integer ann_id);
    // 根据id更新记录
    int updateByPrimaryKeySelective(Announcement record);
    // 分页查询
    List<Announcement> findByPager(Map<String, Object> params);
    // 汇总数据
    long count();
    // 模糊查询
    List<Announcement> findByKey(Map<String, Object> params);

    int insert(Announcement record);
    int updateByPrimaryKey(Announcement record);
}