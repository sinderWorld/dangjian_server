package com.dangjian.dangjian_server.dao;

import com.dangjian.dangjian_server.pojo.Announcement;
import com.dangjian.dangjian_server.pojo.Status;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * 固定信息收集模板表_信息收集状态管理表
 */
@Mapper
public interface StatusDao {
    // 根据id删除一条记录
    int deleteByPrimaryKey(Integer sta_id);

    int insert(Status record);
    // 发布信息收集 添加一条新的记录
    int insertSelective(Status record);
    // 根据id查询一条记录
    Status selectByPrimaryKey(Integer sta_id);
    // 分页查询
    List<Status> findByPager(Map<String, Object> params);
    // 汇总数据
    long count();
    // 模糊查询
    List<Status> findByKey(Map<String, Object> params);
    // 更新记录
    int updateByPrimaryKeySelective(Status record);

    int updateByPrimaryKey(Status record);
}