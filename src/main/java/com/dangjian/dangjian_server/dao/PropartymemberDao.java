package com.dangjian.dangjian_server.dao;


import com.dangjian.dangjian_server.pojo.Activist;
import com.dangjian.dangjian_server.pojo.Propartymember;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * 固定信息收集模板表_预备党员信息收集表
 */
@Mapper
public interface PropartymemberDao {
    // 校验是否已经存在该学生
    Propartymember checkIsStu(int sta_id, String stuNumber);
    // 移除一条记录
    int deleteByPrimaryKey(String stuNumber);
    // 分页查询
    List<Propartymember> findByPager(Map<String, Object> params);
    // 汇总数据
    long count(int sta_id);

    int deleteByPrimaryKey(Integer proId);

    int insert(Propartymember record);
    // 添加一条记录
    int insertSelective(Propartymember record);

    Propartymember selectByPrimaryKey(Integer proId);
    // 更新记录 学生填写
    int updateByPrimaryKeySelective(Propartymember record);

    int updateByPrimaryKey(Propartymember record);

    Propartymember selectInfoByNumber(int sta_id,String stuNumber);
}