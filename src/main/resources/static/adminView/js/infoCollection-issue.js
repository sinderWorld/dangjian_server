// 预加载
$(document).ready(function () {
    layui.use(['laypage', 'layer', 'table', 'laydate','form'], function() {
        var form = layui.form
            ,laypage = layui.laypage
            ,layer = layui.layer
            ,table = layui.table
            ,laydate = layui.laydate
    })
})
// 监听信息发布按钮 弹出窗口
function btuInfoIssueOpen(tb_id) {
    // 发布的信息收集类型
    // 0表示积极分子，1表示发展对象，2表示预备党员
    console.log(tb_id);
    layer.open({
        type: 0,
        title: "发布收集模板",
        anim: 1,
        closeBtn: 2,
        fixed: false,
        area: ['600px','500px'],
        shadeClose: false,
        content: '<div class="container layui-form">\n' +
            '        <div class="layui-form-item">\n' +
            '            <label class="layui-form-label">收集标题</label>\n' +
            '            <div class="layui-input-block">\n' +
            '                <input type="text" name="sj001" id="sj001" lay-verify="required" lay-reqtext="标题不能为空" autocomplete="off" class="layui-input">\n' +
            '            </div>\n' +
            '        </div>\n' +
            '        <div class="layui-form-item">\n' +
            '            <div class="layui-inline">\n' +
            '                <label class="layui-form-label">收集状态</label>\n' +
            '                <div class="layui-input-inline">\n' +
            '                    <select name="status" id="status" lay-filter="sex" lay-verify="required" lay-search="">\n' +
            '                        <option value="1" selected="selected">正在收集</option>\n' +
            '                        <option value="0">停止收集</option>\n' +
            '                    </select>\n' +
            '                </div>\n' +
            '            </div>\n' +
            '        </div>\n' +
            '        <div class="layui-form-item">\n' +
            '            <div class="layui-inline">\n' +
            '                <label class="layui-form-label">截止时间</label>\n' +
            '                <div class="layui-input-inline">\n' +
            '                    <input type="text" name="date" id="date" lay-verify="date" placeholder="yyyyMMdd" autocomplete="off" class="layui-input">\n' +
            '                </div>\n' +
            '            </div>\n' +
            '        </div>\n' +
            '    </div>',
        success: function (layero, index) {
            layui.use(['laypage', 'layer', 'table', 'laydate','form'], function() {
                var form = layui.form
                    ,laypage = layui.laypage
                    ,layer = layui.layer
                    ,table = layui.table
                    ,laydate = layui.laydate

                    // 注册时间
                    laydate.render({
                        elem: '#date',
                        format: 'yyyyMMdd'
                    });
                    // 加载下拉框
                    form.render('select');
            })
            console.log("窗口弹出");
        },
        // 点击确定
        yes: function(index, layero){
            // 获取输入框的值
            var sj001 = $('#sj001').val();
            var status = $('#status').val();
            var createTime = $('#date').val();
            // 先判断里面内容是否为空
            if (sj001 === '' || status === '' || createTime === '') {
                layui.use(['layer'], function() {
                    var layer = layui.layer

                    layer.msg('请勿留空值');
                })
                return false;
            } else {
                $.ajax({
                    type: 'post',
                    url: '/status/addStatusInfo',
                    dataType: "json",
                    traditional: true,
                    contentType : 'application/json;charset=utf-8',
                    data:
                        JSON.stringify({
                            sj001: sj001,
                            status: status,
                            createTime: createTime,
                            tb_id: tb_id
                        }),
                    success: function(res) {
                        layer.alert('发布成功');
                    }
                })
                layer.close(index); //如果设定了yes回调，需进行手工关闭
            }
        },
        cancel: function(index, layero) {
            layer.close(index)
        }
    })
}