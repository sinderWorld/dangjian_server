// 初始加载
$(document).ready(function (){
  // 初始值 获取登录者信息
  f();
});
function f() {
  $.ajax({
    url: "/admin/adminInfo",
    type: "GET",
    success(res) {
      console.log("执行成功:" + res);
      $("#span").html(res.data.account);
    },
    error() {
      layer.msg("网络出错");
    }
  });
}
// 页面控制
function thisPage(url) {
  console.log('页面跳转到---' + url);
  $('#ifr').attr('src',url);
}

// 选择函数 选择基本资料
function myInfo () {
  $('#ifr').attr('src', './myInfo.html');
  $('#this0').attr('class', '');
  $('#this1').attr('class', '');
  $('#this2').attr('class', '');
  $('#this3').attr('class', '');
}
// 退出登录
function exitLogin () {
  console.log("退出登录");
  layui.use(['form','layer'], function () {
    $.ajax({
      url: "/admin/exitLogin",
      type: "GET",
      success(res) {
        console.log("执行成功:" + res);
        // 登录成功
        window.location.href = "login.html";
        layer.msg(res.msg);
      },
      error() {
        layer.msg("网络出错");
      }
    });
  });
};