//预定义
// 定义验证码
var newStr = '<img src="http://localhost:8083/test" onclick="verifyImgLoad()" id="verifyImgNode"/>';
$(document).ready(function () {
  // 验证码显示函数
  function verifyImg() {
    console.log("verifyImg");
    $('#verifyImg').prepend(newStr);
  };
  // 第一次执行验证码显示
  verifyImg();
  //Demo
  layui.use(['form','layer'], function () {
    var form = layui.form,
        layer = layui.layer;
    layer.msg("欢迎登录智慧党建后台管理系统");
    //监听提交
    form.on('submit(formDemo)', function (data) {
      //文字弹窗，获取文本弹窗
      login();
      return false;
    });
    // 重置
    $("#reset").click(function () {
      $("#account").val("");
      $("#password").val("");
      $("#verifyCode").val("");
    });
  });
});
// 验证码显示函数
function verifyImgLoad() {
  console.log("verifyImgLoad");
  // 定义时间戳
  var severtime = new Date();
  var ux = Date.UTC(severtime.getFullYear(),severtime.getMonth(),severtime.getDay(),severtime.getHours(),severtime.getMinutes(),severtime.getSeconds())/1000;
  console.log(ux);
  $("#verifyImg img").remove();
  var str = "<img src=\"http://localhost:8083/test?id=" + ux + "\"" + " onclick=\"verifyImgLoad()\"" + "/>";
  $('#verifyImg').prepend(str);
};

// 登录
function login () {
  layui.use(['form','layer'], function () {
    $.ajax({
      url: "/admin/login",
      type: "POST",
      data: {
        "account": $("#account").val(),
        "password": $("#password").val(),
        "vcCode": $("#verifyCode").val()
      },
      success(res) {
        console.log("执行成功:" + res);
        if (res.status == 0){
          // 登录成功
          window.location.href = "main.html";
        } else {
          $("#account").val("");
          $("#password").val("");
          $("#verifyCode").val("");
          layer.msg(res.msg);
          verifyImgLoad()
        }
      },
      error() {
        console.log("网络出错");
        layer.msg("网络出错");
        $("#account").val("");
        $("#password").val("");
        $("#verifyCode").val("");
      }
    });
  });
};