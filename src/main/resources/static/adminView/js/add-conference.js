// 预加载
$(document).ready(function() {
    layui.use(['form','layer', 'laydate'], function(){
        var form = layui.form
            ,layer = layui.layer
            ,laydate = layui.laydate

        // 时间选择器
        laydate.render({
            elem: '#date',
            format: 'yyyyMMdd'
        });

        form.on('submit(demo1)', function(data){
            addConference();
            return false;
        });
    })
});

// 定义一个函数 用于增加会议记录
function addConference() {
    $.ajax({
        url: '/conference/addConferenceInfo',
        type: 'POST',
        dataType: "json",
        traditional: true,
        contentType : 'application/json;charset=utf-8',
        data:
            JSON.stringify({
                meetingName: $('#meetingName').val(),
                meetingHost: $('#meetingHost').val(),
                meetingTime: $('#date').val(),
                meetingPlace: $('#meetingPlace').val(),
                conferenceTheme: $('#conferenceTheme').val(),
                meetingDesc: $('#meetingDesc').val(),
            }),
        success(res) {
            console.log('新增成功');
            layer.msg(res.msg);
            $('#meetingName').val(''),
            $('#meetingHost').val(''),
            $('#meetingTime').val(''),
            $('#meetingPlace').val(''),
            $('#conferenceTheme').val(''),
            $('#meetingDesc').val('')
        },
        error() {
            console.log('新增失败，请与管理员联系！');
        }
    })
}