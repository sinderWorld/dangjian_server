/**
 * 1 查看 获取相应的sta_id查询数据
 * 2 根据返回的数据生成数据表格
 * 3 数据表格存在一个操作按钮 (移除)
 * 4 "移除" 按钮用于移除信息收集表中该学生
 */
$(document).ready(function () {
    console.log('执行');
    //初始化执行，查询全部数据
    getTableList(inter_url);
});
// 接受页面传参
function getQueryString(name) {
    var reg = new RegExp('(^|&)' + name + '=([^&]*)(&|$)', 'i');
    var r = window.location.search.substr(1).match(reg);
    if (r != null) {
        return unescape(r[2]);
    }
    return null;
}
// 获取sta_id 用于添加学生加入绑定模板表
var sta_id = getQueryString("sta_id");
// 获取tb_id 用户区分获取学生类型
var tb_id = getQueryString("tb_id");

console.log(sta_id + ":" + tb_id)
// 定义接口
const inter_url = "/template/getPageTemplateInfo?tb_id=" + tb_id + "&sta_id=" + sta_id;

// 获取表格数据
function getTableList(inter_url) {
    // LayUI JavaScript代码区域
    layui.use(['laypage', 'layer', 'table', 'laydate'], function () {
        var table = layui.table
            , laypage = layui.laypage //分页
            , layer = layui.layer //弹层;
        // 每页面显示条数
        var limitcount = 10;
        // 当前页
        var curnum = 1;

        // 创建表格
        table.render({
            elem: '#infoMag',
            height: 480,
            url: inter_url,//数据接口
            method: "GET",
            title: '查看',
            page: true, //开启分页
            toolbar: '#toolbarDemo',
            limit: 10,
            limits: [10, 20, 30],
            //toolbar: true,//开启工具栏，此处显示默认图标，可以自定义模板，详见文档
            parseData: function (res) {
                console.log(res);
                var count = 0;
                if (res.data == null) {
                    count = 0;
                } else {
                    count = res.data.count;
                }
                return {
                    "code": res.status, //解析接口状态
                    "msg": res.msg, //解析提示文本
                    "count": count, //解析数据长度
                    "data": res.data.data //解析数据列表
                }
            },
            cols: [
                [ //表头
                    {type: 'checkbox', fixed: 'left'},
                    {
                        field: 'stuName',
                        title: '姓名',
                        width: 180,
                        sort: true,
                        fixed: 'left',
                    },{
                    field: 'stuNumber',
                    title: '学号',
                    width: 280
                }, {
                    field: 'gender',
                    title: '性别',
                    width: 180
                }, {
                    field: 'hometown',
                    title: '籍贯',
                    width: 180
                }, {
                    field: 'hometown',
                    title: '籍贯',
                    width: 180
                }, {
                    fixed: 'right',
                    title: '操作',
                    width: 200,
                    align: 'center',
                    toolbar: '#barArray'
                }, {
                    fixed: 'act_id',
                    hide: true,
                }
                ]
            ],
            done: function (res, curr, count) {
                // 开启分页
                laypage.render({
                    elem: '#infoMag', //分页容器的id
                    count: count, //总条数
                    curr: curr,
                    limit: limitcount, //当前页显示数据
                    skin: '#1E9FFF', //自定义选中色值
                    jump: function (obj, first) {
                        if (!first) {
                            curnum = obj.curr;
                            limitcount = obj.limit;
                            layer.msg('第' + obj.curr + '页', {
                                offset: 'b'
                            });
                        }
                    }
                })
            }
        });

        //监听行工具事件
        table.on('tool(test)', function(obj){
            var data = obj.data;
            //console.log(obj)
            if(obj.event === 'del'){
                layer.confirm('真的移除该学生吗', function(index){
                    console.log('移除学生')
                    $.ajax({
                        type: 'POST',
                        url: '/template/delByStuNumberTemplate',
                        data: {
                            "tb_id": tb_id,
                            "stuNumber": obj.data.stuNumber
                        },
                        success: function (res) {
                            layer.msg(res.msg);
                            // 移除成功
                            getTableList(inter_url);
                        },
                        error: function () {
                            console.log('失败')
                        }
                    })
                    layer.close(index);
                })
            }
        });
    });
};