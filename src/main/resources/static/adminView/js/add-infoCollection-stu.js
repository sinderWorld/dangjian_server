/**
 * 1 获取传递过来的参数
 *      sta_id用于添加记录的时候对应收集表
 *      tb_id用于判断查询返回的学生类型，是积极分子还是发展对象还是党员
 * 2 生成对应表格数据 模糊搜索也支持tb_id搜索
 * 3 标题设置成动态 根据获取的tb_status -> sj001作为展示标题
 * 4 表格数据一个按钮响应事件，"添加" 将学生添加到需要收集的信息中 添加前校验是否已经存在该学生
 */
$(document).ready(function () {
    console.log('执行');
    //初始化执行，查询全部数据
    getTableList(inter_url);
    // 初始化渲染条件选择框
    // getOptionList();
    // layUI
    layui.use('form', function() {
        var form = layui.form // 表单
        // 模糊搜索
        form.on('submit(formDemo)', function (data) {
            // 设置参数 及 请求接口
            const inter_url = "/student/getKeyWorkStudentInfoBy?keyWork=" + data.field.keyword + "&tb_id=" + tb_id;
            layer.msg("搜索中...");
            getTableList(inter_url);
            return false;
        });
    })
});
// 接受页面传参
function getQueryString(name) {
    var reg = new RegExp('(^|&)' + name + '=([^&]*)(&|$)', 'i');
    var r = window.location.search.substr(1).match(reg);
    if (r != null) {
        return unescape(r[2]);
    }
    return null;
}
// 获取sta_id 用于添加学生加入绑定模板表
var sta_id = getQueryString("sta_id");
// 获取tb_id 用户区分获取学生类型
var tb_id = getQueryString("tb_id");
// 定义接口
const inter_url = "/student/getPageStudentInfoBy?tb_id=" + tb_id;
// 获取对应类型数据 获取相应的学生渲染表格数据
// 获取表格数据
function getTableList(inter_url) {
    // 执行先清空输入框内容
    $("#keyword").val("")
    // LayUI JavaScript代码区域
    layui.use(['laypage', 'layer', 'table', 'laydate'], function () {
        var table = layui.table
            , laypage = layui.laypage //分页
            , layer = layui.layer //弹层;
        // 每页面显示条数
        var limitcount = 10;
        // 当前页
        var curnum = 1;

        // 创建表格
        table.render({
            elem: '#infoMag',
            height: 480,
            url: inter_url,//数据接口
            method: "GET",
            title: '添加学生',
            page: true, //开启分页
            toolbar: '#toolbarDemo',
            limit: 10,
            limits: [10, 20, 30],
            //toolbar: true,//开启工具栏，此处显示默认图标，可以自定义模板，详见文档
            parseData: function (res) {
                console.log(res);
                var count = 0;
                if (res.data == null) {
                    count = 0;
                } else {
                    count = res.data.count;
                }
                return {
                    "code": res.status, //解析接口状态
                    "msg": res.msg, //解析提示文本
                    "count": count, //解析数据长度
                    "data": res.data.data //解析数据列表
                }
            },
            cols: [
                [ //表头
                    {type: 'checkbox', fixed: 'left'},
                    {
                        field: 'stuName',
                        title: '姓名',
                        width: 180,
                        sort: true,
                        fixed: 'left',
                    },{
                    field: 'stuNumber',
                    title: '学号',
                    width: 280
                }, {
                    field: 'gender',
                    title: '性别',
                    width: 180
                }, {
                    field: 'branch',
                    title: '所在党支部',
                    width: 240
                },{
                    field: 'xy001',
                    title: '所在学院',
                    width: 240
                }, {
                    field: 'zy001',
                    title: '所在专业',
                    width: 240
                }, {
                    field: 'bj001',
                    title: '班级',
                    width: 240
                }, {
                    fixed: 'right',
                    title: '操作',
                    width: 200,
                    align: 'center',
                    toolbar: '#barArray'
                }, {
                    fixed: 'stu_id',
                    hide: true,
                }
                ]
            ],
            done: function (res, curr, count) {
                // 开启分页
                laypage.render({
                    elem: '#infoMag', //分页容器的id
                    count: count, //总条数
                    curr: curr,
                    limit: limitcount, //当前页显示数据
                    skin: '#1E9FFF', //自定义选中色值
                    jump: function (obj, first) {
                        if (!first) {
                            curnum = obj.curr;
                            limitcount = obj.limit;
                            layer.msg('第' + obj.curr + '页', {
                                offset: 'b'
                            });
                        }
                    }
                })
                // 判断数据
                // $("[data-field='status']").children().each(function(){
                //     if($(this).text()=='0'){
                //         $(this).text("已停止")
                //     }else if($(this).text()=='1'){
                //         $(this).text("正在收集")
                //     }
                // });
                // $("[data-field='tb_id']").children().each(function(){
                //     if($(this).text()=='0'){
                //         $(this).text("积极分子信息收集")
                //     }else if($(this).text()=='1'){
                //         $(this).text("发展对象信息收集")
                //     }else if ($(this).text()=='2') {
                //         $(this).text("预备党员信息收集")
                //     }
                // });
            }
        });

        //监听行工具事件
        table.on('tool(test)', function(obj){
            var data = obj.data;
            //console.log(obj)
            if(obj.event === 'add'){
                console.log('添加学生')
                // 添加学生之前
                // 先校验学生是否已经存在
                console.log(obj.data.stuNumber);
                console.log(sta_id);
                console.log(tb_id);
                $.ajax({
                    type: 'POST',
                    url: '/template/checkTemplate',
                    data: {
                        "sta_id": sta_id,
                        "tb_id": tb_id,
                        "stuNumber": obj.data.stuNumber
                    },
                    success: function (res) {
                        // 校验通过 就执行添加
                        if (res.status === 1) {
                            layer.msg(res.msg);
                        } else {
                            // 执行添加
                            $.ajax({
                                type: 'POST',
                                url: '/template/addTemplate',
                                data: {
                                    "sta_id": sta_id,
                                    "tb_id": tb_id,
                                    "stuName": obj.data.stuName,
                                    "stuNumber": obj.data.stuNumber,
                                    "gender": obj.data.gender
                                },
                                success: function (res) {
                                    layer.msg(res.msg);
                                },
                                error: function () {
                                    layer.msg(res.msg)
                                }
                            })
                        }
                    },
                    error: function () {
                        console.log('校验不通过')
                    }
                })
            }
        });
    });
};
