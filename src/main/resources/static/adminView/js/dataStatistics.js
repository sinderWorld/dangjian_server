// 预加载数据信息
getCountInfo();

// LayUI JavaScript代码区域
layui.use('element', function () {
    var element = layui.element;
});

// 预加载
// $(document).ready(function () {
//
// });

// 获取汇总数据
function getCountInfo () {
    $.ajax({
        url: "/count/dataCount",
        type: "GET",
        success(res) {
            console.log(res);
            showData(res.data);
        },
        error() {
            console.log("网络出错，请刷新重试");
        }
    })
};

// 信息渲染
function showData (resData) {
    layui.use('laytpl', function () {
        var laytpl = layui.laytpl;
        var myDatas = {
            //数据
            "resData": resData
        };
        var getTpl = countData.innerHTML, view = $("#countDataTpl");
        laytpl(getTpl).render(myDatas, function (result) {
            //清空元素内部的html代码
            view.empty();
            //重新添加
            view.append(result);
        });
    })
};