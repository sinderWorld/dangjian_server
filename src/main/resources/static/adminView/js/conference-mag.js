// 定义接口
const inter_url = "/conference/getPageConferenceInfo";

// 预加载
$(document).ready(function () {
    console.log('执行');
    //初始化执行，查询全部数据
    getTableList(inter_url);
    // 初始化渲染条件选择框
    // getOptionList();
    // layUI
    layui.use('form', function() {
        var form = layui.form // 表单
        // 模糊搜索
        form.on('submit(formDemo)', function (data) {
            // 设置参数 及 请求接口
            const inter_url = "/conference/getKeyWorkConferenceInfo?keyWork=" + data.field.keyword;
            layer.msg("搜索中...");
            getTableList(inter_url);
            return false;
        });
    })
});

// 获取表格数据
function getTableList(inter_url) {
    // 执行先清空输入框内容
    $("#keyword").val("")
    // LayUI JavaScript代码区域
    layui.use(['laypage', 'layer', 'table', 'laydate'], function () {
        var table = layui.table
            , laypage = layui.laypage //分页
            , layer = layui.layer //弹层;
            , laydate = layui.laydate

        laydate.render({
            elem: '#date1'
        });

        // 每页面显示条数
        var limitcount = 10;
        // 当前页
        var curnum = 1;

        // 创建表格
        table.render({
            elem: '#infoMag',
            height: 480,
            url: inter_url,//数据接口
            method: "GET",
            title: '支部会议信息',
            page: true, //开启分页
            toolbar: '#toolbarDemo',
            limit: 10,
            limits: [10, 20, 30],
            //toolbar: true,//开启工具栏，此处显示默认图标，可以自定义模板，详见文档
            parseData: function (res) {
                console.log(res);
                var count = 0;
                if (res.data == null) {
                    count = 0;
                } else {
                    count = res.data.count;
                }
                return {
                    "code": res.status, //解析接口状态
                    "msg": res.msg, //解析提示文本
                    "count": count, //解析数据长度
                    "data": res.data.data //解析数据列表
                }
            },
            cols: [
                [ //表头
                    {type: 'checkbox', fixed: 'left'},
                    {
                        field: 'meetingName',
                        title: '会议名称',
                        width: 120,
                        sort: true,
                        fixed: 'left',
                    }, {
                    field: 'meetingHost',
                    title: '会议主持人',
                    width: 140
                }, {
                    field: 'meetingTime',
                    title: '会议时间',
                    width: 160,
                    sort: true,
                }, {
                    field: 'meetingPlace',
                    title: '会议地点',
                    width: 180,
                    sort: true
                }, {
                    field: 'conferenceTheme',
                    title: '会议主题',
                    width: 180,
                    sort: true
                }, {
                    field: 'meetingDesc',
                    title: '会议说明',
                    width: 160,
                    sort: true
                }, {
                    fixed: 'right',
                    title: '操作',
                    width: 200,
                    align: 'center',
                    toolbar: '#barArray'
                }, {
                    fixed: 'con_id',
                    hide: true,
                }, {
                    fixed: 'adm_id',
                    hide: true,
                }
                ]
            ],
            done: function (res, curr, count) {
                // 开启分页
                laypage.render({
                    elem: '#infoMag', //分页容器的id
                    count: count, //总条数
                    curr: curr,
                    limit: limitcount, //当前页显示数据
                    skin: '#1E9FFF', //自定义选中色值
                    jump: function (obj, first) {
                        if (!first) {
                            curnum = obj.curr;
                            limitcount = obj.limit;
                            layer.msg('第' + obj.curr + '页', {
                                offset: 'b'
                            });
                        }
                    }
                })
            }
        });

        //头工具栏事件
        table.on('toolbar(test)', function(obj){
            var checkStatus = table.checkStatus(obj.config.id);
            switch(obj.event){
                case 'getCheckData':
                    var data = checkStatus.data;
                    if (data.length == 0) {
                        layer.alert('请先勾选再删除');
                        console.log(data.length);
                        return;
                    }
                    layer.confirm('是否确认批量删除？',function(index){
                        var array = new Array();
                        for (var i = 0;i < data.length;i++){
                            array.push(data[i].con_id);
                        }
                        $.ajax({
                            type:'post',
                            url: "/conference/delByBatchCon",
                            dataType:'json',
                            traditional:true,//这个参数必须添加，采用传统方式转换
                            data: {
                                "array": array
                            },
                            success:function(response){
                                var code=response.data;
                                if (code >= 1) {
                                    layer.alert(JSON.stringify("删除了  " + code + "  条的记录！"));
                                    // 刷新当前界面
                                    getTableList("/conference/getPageConferenceInfo");
                                } else {
                                    layer.alert(JSON.stringify("删除失败"));
                                }
                            }
                        })
                        // layer.alert(JSON.stringify(data));
                        layer.close(index);
                    })
                    break;

                //自定义头工具栏右侧图标 - 提示
                case 'LAYTABLE_TIPS':
                    layer.alert('这是工具栏右侧自定义的一个图标按钮');
                    break;
            };
        });

        //监听行工具事件
        table.on('tool(test)', function(obj){
            var data = obj.data;
            //console.log(obj)
            if(obj.event === 'del'){
                layer.confirm('真的删除行么', function(index){
                    var meetingName = data.meetingName;
                    $.ajax({
                        type:'post',
                        url: "/conference/delByIdCon",
                        data: {
                            "con_id": data.con_id
                        },
                        success:function(response){
                            var code=response.data;
                            if (code == 1) {
                                layer.alert(JSON.stringify("删除了" + meetingName + "的记录！"));
                                // 刷新当前界面
                                getTableList("/conference/getPageConferenceInfo");
                            } else {
                                layer.alert(JSON.stringify("删除失败"));
                            }
                        }
                    })
                    layer.close(index);
                });
            }
        });
    });
};
