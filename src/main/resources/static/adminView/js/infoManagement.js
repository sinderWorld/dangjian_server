// 定义接口
const inter_url = "/student/getPageStudentInfo";

// 预加载
$(document).ready(function () {
    console.log('执行');
    //初始化执行，查询全部数据
    getTableList(inter_url);
    // 初始化渲染条件选择框
    // getOptionList();
    // layUI
    layui.use('form', function() {
        var form = layui.form // 表单
        // 模糊搜索
        form.on('submit(formDemo)', function (data) {
            // 设置参数 及 请求接口
            const inter_url = "/student/getKeyWorkStudentInfo?keyWork=" + data.field.keyword;
            layer.msg("搜索中...");
            getTableList(inter_url);
            return false;
        });

        // 加载表单_搜索选择框
        // form.render('select');
        getOptionList(form,"xy001");
        getOptionList(form,"zy001");
        getOptionList(form,"bj001");
        // select下拉框选中触发事件
        // 联动选择框
        // 学院
        form.on("select(xy)", function(data){
            var inter_url = "/student/getPageStudentInfo";
            layer.msg("搜索中...");
            if (data.value == 0) {
                getTableList(inter_url);
            } else {
                // 设置参数 及 请求接口
                inter_url = "/student/findByCondition?col=" + "xy001" + "&code=" + data.value;
                getTableList(inter_url);
            }
            //alert(data.value); // 获取选中的值
        });
        // 专业
        form.on("select(zy)", function(data){
            var inter_url = "/student/getPageStudentInfo";
            layer.msg("搜索中...");
            if (data.value == 0) {
                getTableList(inter_url);
            } else {
                // 设置参数 及 请求接口
                inter_url = "/student/findByCondition?col=" + "zy001" + "&code=" + data.value;
                getTableList(inter_url);
            }
            //alert(data.value); // 获取选中的值
        });
        // 班级
        form.on("select(bj)", function(data){
            var inter_url = "/student/getPageStudentInfo";
            layer.msg("搜索中...");
            if (data.value == 0) {
                getTableList(inter_url);
            } else {
                // 设置参数 及 请求接口
                inter_url = "/student/findByCondition?col=" + "bj001" + "&code=" + data.value;
                getTableList(inter_url);
            }
            //alert(data.value); // 获取选中的值
        });
    })
});
// 动态渲染脚本类型下拉框
// 1.发送ajax请求得到data
// 2.将data渲染到页面上
function getOptionList(form,belong) {
    // 请求
    $.ajax({
        type:'get',
        url: "/dict/getByBelong?belong=" + belong,
        success:function(response){
            var data=response.data;
            console.log(data);
            var t = '<option value="0" selected="selected">全部</option>';
            if(data.length == 0){
                return;
            } else {
                for (i=0;i<data.length;i++) {
                    t += "<option value='"+data[i].code+"'>"+data[i].showvalue+"</option>";
                    console.log(t);
                }
            };
            // 选择执行
            switch (belong) {
                case 'xy001':
                    $("#xy").html("");
                    $("#xy").append(t);
                    console.log("sss")
                    break;
                case 'zy001':
                    $("#zy").html("");
                    $("#zy").append(t);
                    console.log("ssss")
                    break;
                case 'bj001':
                    $("#bj").html("");
                    $("#bj").append(t);
                    console.log("sssss")
                    break;
            }
            form.render('select');
        }
    })
}

// 获取表格数据
function getTableList(inter_url) {
    // 执行先清空输入框内容
    $("#keyword").val("")
    // LayUI JavaScript代码区域
    layui.use(['laypage', 'layer', 'table', 'laydate'], function () {
        var table = layui.table
            , laypage = layui.laypage //分页
            , layer = layui.layer //弹层;
            , laydate = layui.laydate

        laydate.render({
            elem: '#date1'
        });

        // 每页面显示条数
        var limitcount = 10;
        // 当前页
        var curnum = 1;

        // 创建表格
        table.render({
            elem: '#infoMag',
            height: 480,
            url: inter_url,//数据接口
            method: "GET",
            title: '党员信息',
            page: true, //开启分页
            toolbar: '#toolbarDemo',
            limit: 10,
            limits: [10, 20, 30],
            //toolbar: true,//开启工具栏，此处显示默认图标，可以自定义模板，详见文档
            parseData: function (res) {
                console.log(res);
                var count = 0;
                if (res.data == null) {
                    count = 0;
                } else {
                    count = res.data.count;
                }
                return {
                    "code": res.status, //解析接口状态
                    "msg": res.msg, //解析提示文本
                    "count": count, //解析数据长度
                    "data": res.data.data //解析数据列表
                }
            },
            cols: [
                [ //表头
                    {type: 'checkbox', fixed: 'left'},
                    {
                        field: 'stuName',
                        title: '姓名',
                        width: 120,
                        sort: true,
                        fixed: 'left',
                    }, {
                    field: 'stuNumber',
                    title: '学号',
                    width: 140
                }, {
                    field: 'branch',
                    title: '所在支部',
                    width: 160,
                    sort: true,
                }, {
                    field: 'xy001',
                    title: '学院',
                    width: 180,
                    sort: true
                }, {
                    field: 'zy001',
                    title: '专业',
                    width: 180,
                    sort: true
                }, {
                    field: 'bj001',
                    title: '班级',
                    width: 160,
                    sort: true
                }, {
                    field: 'gender',
                    title: '性别',
                    width: 80,
                    sort: true
                }, {
                    field: 'phone',
                    title: '联系方式',
                    width: 160,
                    sort: true
                }, {
                    field: 'nation',
                    title: '民族',
                    width: 80,
                    sort: true
                }, {
                    field: 'hometown',
                    title: '籍贯',
                    width: 110,
                    sort: true,
                }, {
                    field: 'idenNumber',
                    title: '身份证号码',
                    width: 180
                }, {
                    field: 'partyAppliTime',
                    title: '入党申请时间',
                    width: 120
                }, {
                    fixed: 'right',
                    title: '操作',
                    width: 200,
                    align: 'center',
                    toolbar: '#barArray'
                }, {
                    fixed: 'stu_id',
                    hide: true,
                }
                ]
            ],
            done: function (res, curr, count) {
                // 开启分页
                laypage.render({
                    elem: '#infoMag', //分页容器的id
                    count: count, //总条数
                    curr: curr,
                    limit: limitcount, //当前页显示数据
                    skin: '#1E9FFF', //自定义选中色值
                    jump: function (obj, first) {
                        if (!first) {
                            curnum = obj.curr;
                            limitcount = obj.limit;
                            layer.msg('第' + obj.curr + '页', {
                                offset: 'b'
                            });
                        }
                    }
                })
            }
        });

        //头工具栏事件
        table.on('toolbar(test)', function(obj){
            var checkStatus = table.checkStatus(obj.config.id);
            switch(obj.event){
                case 'getCheckData':
                    var data = checkStatus.data;
                    if (data.length == 0) {
                        layer.alert('请先勾选再删除');
                        console.log(data.length);
                        return;
                    }
                    layer.confirm('是否确认批量删除？',function(index){
                        var array = new Array();
                        for (var i = 0;i < data.length;i++){
                            array.push(data[i].stu_id);
                        }
                        $.ajax({
                            type:'post',
                            url: "/student/delByBatchStu",
                            dataType:'json',
                            traditional:true,//这个参数必须添加，采用传统方式转换
                            data: {
                                "array": array
                            },
                            success:function(response){
                                var code=response.data;
                                if (code >= 1) {
                                    layer.alert(JSON.stringify("删除了  " + code + "  条的记录！"));
                                    // 刷新当前界面
                                    getTableList("/student/getPageStudentInfo");
                                } else {
                                    layer.alert(JSON.stringify("删除失败"));
                                }
                            }
                        })
                    // layer.alert(JSON.stringify(data));
                        layer.close(index);
                    })
                    break;

                //自定义头工具栏右侧图标 - 提示
                case 'LAYTABLE_TIPS':
                    layer.alert('这是工具栏右侧自定义的一个图标按钮');
                    break;
            };
        });

        //监听行工具事件
        table.on('tool(test)', function(obj){
            var data = obj.data;
            //console.log(obj)
            if(obj.event === 'del'){
                layer.confirm('真的删除行么', function(index){
                    var stuName = data.stuName;
                    $.ajax({
                        type:'post',
                        url: "/student/delByIdStu",
                        data: {
                            "stuId": data.stu_id
                        },
                        success:function(response){
                            var code=response.data;
                            if (code == 1) {
                                layer.alert(JSON.stringify("删除了" + stuName + "的记录！"));
                                // 刷新当前界面
                                getTableList("/student/getPageStudentInfo");
                            } else {
                                layer.alert(JSON.stringify("删除失败"));
                            }
                        }
                    })
                    layer.close(index);
                });
            } else if(obj.event === 'edit'){
                openUpdate(obj.data.stu_id);
                // layer.close(index);
            } else if(obj.event === 'query') {
                openQuery(obj.data.stu_id);
                // layer.alert(JSON.stringify(obj.data));
            }
        });
    });
};
// 查看窗口弹出
function openQuery (id) {
    $.ajax({
        type: "get",
        url: "/student/getByIdStu",
        data: {
            "stu_id": id
        },
        success: function(res) {
            layer.open({
                type: 0,
                title: "查看信息",
                anim: 4,
                closeBtn: 2,
                fixed: false,
                area: ['800px','400px'],
                shadeClose: false,
                content: '<table class="layui-table">\n' +
                    '  <colgroup>\n' +
                    '    <col width="150">\n' +
                    '    <col width="200">\n' +
                    '    <col>\n' +
                    '  </colgroup>\n' +
                    '  <thead>\n' +
                    '    <tr>\n' +
                    '      <th>名称值</th>\n' +
                    '      <th>时间</th>\n' +
                    '    </tr> \n' +
                    '  </thead>\n' +
                    '  <tbody>\n' +
                    '    <tr>\n' +
                    '      <td>入党申请时间</td>\n' +
                    '      <td>' + res.data.partyAppliTime + '</td>\n' +
                    '    </tr>\n' +
                    '    <tr>\n' +
                    '      <td>积极分子结业时间</td>\n' +
                    '      <td>' + res.data.partyActivistTime + '</td>\n' +
                    '    </tr>\n' +
                    '    <tr>\n' +
                    '      <td>发展对象结业时间</td>\n' +
                    '      <td>' + res.data.depTargetTime + '</td>\n' +
                    '    </tr>\n' +
                    '    <tr>\n' +
                    '      <td>成为预备党员时间</td>\n' +
                    '      <td>' + res.data.partyTime + '</td>\n' +
                    '    </tr>\n' +
                    '    <tr>\n' +
                    '      <td>党员转正时间</td>\n' +
                    '      <td>' + res.data.turnPositiveTime + '</td>\n' +
                    '    </tr>\n' +
                    '  </tbody>\n' +
                    '</table>',
                success: function (layero, index) {
                    console.log("窗口弹出");
                },
            })
        },
        error: function (e) {
            layer.alert("查询数据出现错误，请联系管理员");
        }
    })
};
// 弹出编辑窗口
function openUpdate (id) {
    $.ajax({
        type: "get",
        url: "/student/getByIdStu",
        data: {
            "stu_id": id
        },
        success: function(res) {
            layer.open({
                type: 0,
                title: "编辑信息",
                anim: 1,
                closeBtn: 2,
                fixed: false,
                area: ['500px','650px'],
                shadeClose: false,
                content: '<table class="layui-table">\n' +
                    '  <colgroup>\n' +
                    '    <col width="450">\n' +
                    '    <col>\n' +
                    '  </colgroup>\n' +
                    '  <tbody>\n' +
                    '    <tr>\n' +
                    '<td>' +
                    '  <div class="layui-form-item">\n' +
                    '    <div class="layui-inline">\n' +
                    '      <label class="layui-form-label">入党申请时间</label>\n' +
                    '      <div class="layui-input-block">\n' +
                    '        <input type="text" name="date" id="partyAppliTime" autocomplete="off" class="layui-input" value=' + res.data.partyAppliTime + '>\n' +
                    '      </div>\n' +
                    '    </div>\n' +
                    '  </div>' +
                    '</td>\n' +
                    '    </tr>\n' +
                    '    <tr>\n' +
                    '<td>' +
                    '  <div class="layui-form-item">\n' +
                    '    <div class="layui-inline">\n' +
                    '      <label class="layui-form-label">入党积极分子时间</label>\n' +
                    '      <div class="layui-input-block">\n' +
                    '        <input type="text" name="date" id="partyActivistTime" autocomplete="off" class="layui-input" value="' + res.data.partyActivistTime + '">\n' +
                    '      </div>\n' +
                    '    </div>\n' +
                    '  </div>' +
                    '</td>\n' +
                    '    </tr>\n' +
                    '    <tr>\n' +
                    '<td>' +
                    '  <div class="layui-form-item">\n' +
                    '    <div class="layui-inline">\n' +
                    '      <label class="layui-form-label">发展对象时间</label>\n' +
                    '      <div class="layui-input-block">\n' +
                    '        <input type="text" name="date" id="depTargetTime" autocomplete="off" class="layui-input" value="' + res.data.depTargetTime + '">\n' +
                    '      </div>\n' +
                    '    </div>\n' +
                    '  </div>' +
                    '</td>\n' +
                    '    </tr>\n' +
                    '    <tr>\n' +
                    '<td>' +
                    '  <div class="layui-form-item">\n' +
                    '    <div class="layui-inline">\n' +
                    '      <label class="layui-form-label">成为预备党员时间</label>\n' +
                    '      <div class="layui-input-block">\n' +
                    '        <input type="text" name="date" id="partyTime" autocomplete="off" class="layui-input" value="' + res.data.partyTime + '">\n' +
                    '      </div>\n' +
                    '    </div>\n' +
                    '  </div>' +
                    '</td>\n' +
                    '    </tr>\n' +
                    '    <tr>\n' +
                    '<td>' +
                    '  <div class="layui-form-item">\n' +
                    '    <div class="layui-inline">\n' +
                    '      <label class="layui-form-label">党员转正时间</label>\n' +
                    '      <div class="layui-input-block">\n' +
                    '        <input type="text" name="date" id="turnPositiveTime" autocomplete="off" class="layui-input" value="' + res.data.turnPositiveTime + '">\n' +
                    '      </div>\n' +
                    '    </div>\n' +
                    '  </div>' +
                    '</td>\n' +
                    '    </tr>\n' +
                    '  </tbody>\n' +
                    '</table>',
                success: function (layero, index) {
                    console.log("窗口弹出");
                },
                // 确定按钮执行函数
                yes: function(index, layero){
                    //do something
                    // console.log($('#partyAppliTime').val());
                    $.ajax({
                        type: 'post',
                        url: '/student/updateByIdStu',
                        dataType: "json",
                        traditional: true,
                        contentType : 'application/json;charset=utf-8',
                        data:
                            JSON.stringify({
                                stu_id: id,
                                partyAppliTime: $('#partyAppliTime').val(),
                                partyActivistTime: $('#partyActivistTime').val(),
                                depTargetTime: $('#depTargetTime').val(),
                                partyTime: $('#partyTime').val(),
                                turnPositiveTime: $('#turnPositiveTime').val(),
                        }),
                        success: function(res) {
                            layer.alert('更新成功');
                            getTableList("/student/getPageStudentInfo");
                        }
                    })
                    layer.close(index); //如果设定了yes回调，需进行手工关闭
                }
            })
        }
    })
};
