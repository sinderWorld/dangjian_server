// 预加载
$(document).ready(function() {
    layui.use(['form','layer'], function(){
        var form = layui.form
            ,layer = layui.layer

        //自定义验证规则
        form.verify({
            title: function(value){
                if(value.length < 5){
                    return '标题至少得5个字符啊';
                }
            }
            ,pass: [
                /^[\S]{6,12}$/
                ,'密码必须6到12位，且不能出现空格'
            ]
            ,content: function(value){
                layedit.sync(editIndex);
            }
        });

        form.on('submit(demo1)', function(data){
            upPassword();
            return false;
        });
    })
});
var password; // 预存密码用于校验
var adminId; // 用于保存admin_id
// 获取用户基本信息
function getInfo() {
    $.ajax({
        url: '/admin/adminInfo',
        type: 'GET',
        success(res) {
            console.log(res);
            if (res.status == 1) {
                layer.msg(res.msg);
            } else {
                layer.msg("进入个人编辑页面");
                $('#account').val(res.data.account);
                $('#qx001').val(res.data.qx001Value);
                password = res.data.password;
                adminId = res.data.adm_id;
            }
        },
        error () {
            console.log("网络出错，请刷新重试");
        }
    })
}
getInfo();
// 执行验证 请求更改密码的请求
function upPassword(){
    // 先校验判断
    if (password != $('#password').val()) {
        layer.msg("原密码不正确，请重新输入");
        $('#password').val('')
    } else if ($('#newPassword').val() != $('#newPassword1').val()) {
        layer.msg("新密码与确认密码要一致！！！");
        $('#newPassword').val('')
        $('#newPassword1').val('')
    } else {
        $.ajax({
            url: '/admin/upPassword',
            type: 'POST',
            dataType: "json",
            traditional: true,
            contentType : 'application/json;charset=utf-8',
            data:
                JSON.stringify({
                    adm_id: adminId,
                    password: $('#newPassword').val()
                }),
            success(res) {
                console.log('更新成功');
                $('#password').val('')
                $('#newPassword').val('')
                $('#newPassword1').val('')
                layer.msg(res.msg);
                // 退出登录
                exitLogin();
            }
        })
    }
}

// 退出登录
function exitLogin () {
    console.log("退出登录");
    layui.use(['form','layer'], function () {
        $.ajax({
            url: "/admin/exitLogin",
            type: "GET",
            success(res) {
                console.log("执行成功:" + res);
                // 登录成功
                parent.location.href = "/adminView/login.html";
                layer.msg(res.msg);
            },
            error() {
                layer.msg("网络出错");
            }
        });
    });
};