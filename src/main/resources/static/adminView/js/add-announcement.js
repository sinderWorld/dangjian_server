// 预加载
$(document).ready(function() {
    layui.use(['form','layer', 'laydate'], function(){
        var form = layui.form
            ,layer = layui.layer
            ,laydate = layui.laydate

        form.on('submit(demo1)', function(data){
            addAnnouncemen();
            return false;
        });
    })
});

// 定义一个函数 用于增加会议记录
function addAnnouncemen() {
    $.ajax({
        url: '/announcement/addAnnouncementInfo',
        type: 'POST',
        dataType: "json",
        traditional: true,
        contentType : 'application/json;charset=utf-8',
        data:
            JSON.stringify({
                annTitle: $('#annTitle').val(),
                briefAnn: $('#briefAnn').val(),
                annContent: $('#annContent').val()
            }),
        success(res) {
            console.log('新增成功');
            layer.msg(res.msg);
            $('#annTitle').val(''),
            $('#briefAnn').val(''),
            $('#annContent').val('')
        },
        error() {
            console.log('新增失败，请与管理员联系！');
        }
    })
}