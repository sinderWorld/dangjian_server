// 先查询原本里面的信息
// 再添加
// 预加载
$(document).ready(function() {
    layui.use(['form','laydate'], function() {
        var form = layui.form // 表单
            ,laydate = layui.laydate

        getInfo(sta_id,stuNumber);

        laydate.render({
            elem: '#partyTime'
            ,format: 'yyyyMMdd'
        });
        laydate.render({
            elem: '#partyActivistTime'
            ,format: 'yyyyMMdd'
        });
        laydate.render({
            elem: '#depTargetTime'
            ,format: 'yyyyMMdd'
        });

        // 监听提交
        form.on('submit(demo1)', function(data){
            $.ajax({
                type: "post",
                url: "/template/propartymemberAdd",
                dataType: "json",
                traditional: true,
                contentType : 'application/json;charset=utf-8',
                data: JSON.stringify(data.field),
                success: function(res) {
                    layer.msg(res.msg);
                    // 成功刷返回首页
                    parent.location.href = "/clientView/index.html";
                },
                error: function() {
                    layer.msg('添加失败，请与管理员联系');
                }
            })
            return false;
        });
    });
})
// 接受传参
function getQueryString(name) {
    var reg = new RegExp('(^|&)' + name + '=([^&]*)(&|$)', 'i');
    var r = window.location.search.substr(1).match(reg);
    if (r != null) {
        return unescape(r[2]);
    }
    return null;
}
var sta_id = getQueryString("sta_id");
var stuNumber = getQueryString("stuNumber");

console.log("输出获取的：" + sta_id + "：" + stuNumber);

// 获取基本信息 学号 姓名 性别
function getInfo(sta_id,stuNumber) {
    $.ajax({
        type: "GET",
        url: "/template/getPropartymember",
        data: {
            "sta_id": sta_id,
            "stuNumber": stuNumber
        },
        success: function (res) {
            $('#sta_id').val(sta_id);
            $('#stuName').val(res.data.stuName);
            $('#stuNumber').val(res.data.stuNumber);
        },
        error: function () {
            console.log("查询失败，请与管理员联系")
        }
    })
}