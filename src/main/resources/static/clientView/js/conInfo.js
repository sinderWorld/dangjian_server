// 预加载
$(document).ready(function() {
    layui.use('form', function() {
        var form = layui.form // 表单

        getConInfo(con_id);
    });
})
// 接受传参
function getQueryString(name) {
    var reg = new RegExp('(^|&)' + name + '=([^&]*)(&|$)', 'i');
    var r = window.location.search.substr(1).match(reg);
    if (r != null) {
        return unescape(r[2]);
    }
    return null;
}
var con_id = getQueryString("con_id");

// 查询公告信息
function getConInfo(con_id) {
    $.ajax({
        type: "get",
        url: "/conference/getByIdCon",
        data: {
            "con_id": con_id
        },
        success: function (res) {
            $('#meetingName').val(res.data.meetingName)
            $('#meetingHost').val(res.data.meetingHost)
            $('#meetingTime').val(res.data.meetingTime)
            $('#meetingPlace').val(res.data.meetingPlace)
            $('#conferenceTheme').val(res.data.conferenceTheme)
            $('#meetingDesc').val(res.data.meetingDesc)
        }
    })
}