$(document).ready(function (){
    layui.use(['flow','layer'], function() {
        var flow = layui.flow //日期
            ,layer = layui.layer
        /*layer弹出一个示例*/
        layer.msg('欢迎进入计算机学院智慧党建平台');
        // 定义当前页数 条数
        var limit = 6;
        flow.load({
            elem: '#LAY_demo1' //流加载容器
            ,scrollElem: '#LAY_demo1' //滚动条所在元素，一般不用填，此处只是演示需要。
            ,done: function(page, next){ //执行下一页的回调
                console.log(page);
                $.ajax({
                    type: 'GET',
                    url: '/announcement/getPageAnnouncementInfo',
                    data: {
                        "page": page,
                        "limit": limit
                    },
                    success: function(res) {
                        console.log(res);
                        // 计算总页数
                        var count = res.data.count > 0 ? ((res.data.count < limit) ? 1 : ((res.data.count % limit) ? (parseInt(res.data.count / limit) + 1) : (res.data.count / limit))) : 0;
                        // 数据长度
                        var lenghts = res.data.data.length;
                        //模拟数据插入
                        setTimeout(function(){
                            var lis = [];
                            for(var i = 0; i < lenghts; i++){
                                lis.push('<div class="layui-bg-gray" style="padding: 10px;">\n' +
                                    '        <div class="layui-row layui-col-space15">\n' +
                                    '            <div class="layui-col-md12">\n' +
                                    '                <div class="layui-card">\n' +
                                    '                    <div class="layui-card-header">' + res.data.data[i].annTitle + '</div>\n' +
                                    '                    <div class="layui-card-body">\n' +
                                    '                        ' + res.data.data[i].briefAnn +
                                    '                        <div class="layui-btn-container">\n' +
                                    '                            <button type="button" class="layui-btn layui-btn-xs" onclick="thisInfo('+ res.data.data[i].ann_id +')">查看详情</button>\n' +
                                    '                        </div>\n' +
                                    '                    </div>\n' +
                                    '                </div>\n' +
                                    '            </div>\n' +
                                    '        </div>\n' +
                                    '    </div>')
                            }
                            //执行下一页渲染，第二参数为：满足“加载更多”的条件，即后面仍有分页
                            //pages为Ajax返回的总页数，只有当前页小于总页数的情况下，才会继续出现加载更多
                            next(lis.join(''), page < count); //假设总页数为 10
                        }, 500);
                    }
                })
            }
        });
    });
})

// 点击查看详情页面
function thisInfo(ann_id) {
    layer.open({
        type: 2,
        title: "公告详情",
        anim: 5,
        // closeBtn: 2,
        fixed: false,
        area: ['450px','350px'],
        shadeClose: false,
        content: ['/clientView/annInfo.html?ann_id=' + ann_id,'no'],
        success: function (layero, index) {
            console.log("窗口弹出");
        },
        cancel: function(index, layero) {
            layer.close(index)
        }
    })
}