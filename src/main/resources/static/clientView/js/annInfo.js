// 预加载
$(document).ready(function() {
    layui.use('form', function() {
        var form = layui.form // 表单

        getAnnInfo(ann_id);
    });
})
// 接受传参
function getQueryString(name) {
    var reg = new RegExp('(^|&)' + name + '=([^&]*)(&|$)', 'i');
    var r = window.location.search.substr(1).match(reg);
    if (r != null) {
        return unescape(r[2]);
    }
    return null;
}
var ann_id = getQueryString("ann_id");

// 查询公告信息
function getAnnInfo(ann_id) {
    $.ajax({
        type: "get",
        url: "/announcement/getByIdAnn",
        data: {
            "ann_id": ann_id
        },
        success: function (res) {
            $('#annTitle').val(res.data.annTitle)
            $('#annContent').val(res.data.annContent)
        }
    })
}