// 预加载
$(document).ready(function () {
    layui.use(['form', 'layedit', 'laydate'], function(){
        var form = layui.form
            ,layer = layui.layer
            ,layedit = layui.layedit
            ,laydate = layui.laydate;

        //日期
        laydate.render({
            elem: '#date'
            ,format: 'yyyyMMdd'
        });

        // 搜索
        form.on('submit(formDemo)', function (data) {
            if (data.field.keyword == '') {
                layer.msg('请先输入学号');
                return false;
            } else {
                // 请求数据
                $.ajax({
                    type:'get',
                    url: "/student/getByStuNumber?stuNumber=" + data.field.keyword,
                    success: function(res) {
                        console.log(res);
                        // 保存学号
                        stuNumber = data.field.keyword;
                        onData(res);
                    }
                })
                return false;
            }
        });
    })
})
// 定义保存学号
var stuNumber = null;
// 渲染数据模板
function onData(res) {
    console.log(res)
    if (res.status == 1) {
        // 没有该用户数据
        layui.use(['laytpl'], function () {
            var laytpl = layui.laytpl

            var myDatas = {
                //数据
                "dataInfo": res.msg,
                "status": 1
            };
            var getTpl = statusData.innerHTML, view = $("#statusInfo");
            laytpl(getTpl).render(myDatas, function (result) {
                //清空元素内部的html代码
                view.empty();
                //重新添加
                view.append(result);
            });
        })
    } else {
        layui.use(['laytpl','form','laydate'], function () {
            var laytpl = layui.laytpl
                ,form = layui.form
                ,laydate = layui.laydate;

            var myDatas = {
                //数据
                "activists": res.data.activists,
                "devobjects": res.data.devobjects,
                "propartymembers": res.data.propartymembers,
                "status": 0
            };
            var getTpl = statusData.innerHTML, view = $("#statusInfo");
            laytpl(getTpl).render(myDatas, function (result) {
                //清空元素内部的html代码
                view.empty();
                //重新添加
                view.append(result);
            });
        })
    }
};
// 跳转到收集信息的页面 tb_id用来判断是0积极分子 1发展对象 2预备党员 判断跳转界面
// 对应三个收集页面 填写添加到对应的表中，根据学号先查询收集表内基本信息 再更新数据
// 根据sta_id and stuNumber 进行更新操作。
function thisInfo(sta_id,tb_id) {
    // 跳转收集的页面
    console.log(sta_id,tb_id);
    console.log(stuNumber);
    if (tb_id == 0) {
        // 跳转到积极分子收集信息界面
        window.location.href = "/clientView/jjfz.html?sta_id=" + sta_id + "&stuNumber=" + stuNumber;
    }else if (tb_id == 1) {
        // 跳转到发展对象收集信息界面
        window.location.href = "/clientView/fzdx.html?sta_id=" + sta_id + "&stuNumber=" + stuNumber;
    } else if (tb_id == 2) {
        // 跳转到预备党员收集信息界面
        window.location.href = "/clientView/ybdy.html?sta_id=" + sta_id + "&stuNumber=" + stuNumber;
    }
}