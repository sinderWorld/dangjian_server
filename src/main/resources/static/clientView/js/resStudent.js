// 预加载
$(document).ready(function() {
    layui.use(['form','laydate','layer'], function() {
        var form = layui.form // 表单
            ,laydate = layui.laydate
            ,layer = layui.layer

        //日期
        laydate.render({
            elem: '#partyAppliTime',
            format: 'yyyyMMdd'
        });
        // 加载下拉框
        form.render('select');
        // 监听事件
        // 加载字典值
        // 不写先

        //监听提交
        form.on('submit(demo1)', function(data){
            // 发送请求添加学生
            $.ajax({
                type: "post",
                url: "/student/addStudentInfo",
                dataType: "json",
                traditional: true,
                contentType : 'application/json;charset=utf-8',
                data: JSON.stringify(data.field),
                success: function(res) {
                    layer.msg(res.msg);
                    // 成功刷新当前页面
                    $('#stuName').val('')
                    $('#stuNumber').val('')
                    $('#nation').val('')
                    $('#idenNumber').val('')
                    $('#partyAppliTime').val('')
                },
                error: function() {
                    layer.msg('添加失败，请与管理员联系');
                }
            })
            return false;
        });
    });
})
